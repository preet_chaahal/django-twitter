from django.conf.urls import url

from . import views

urlpatterns = [
	url(r'^$', views.index, name='home'),
	url(r'^error-reporting/', views.error_reporting, name='error_reporting'),

	url(r'^login/', views.login, name='login'),
	url(r'^logout/', views.logout, name='logout'),
	url(r'^tweet/', views.tweet, name='tweet'),
	url(r'^twitter/accounts/add/', views.twitter_add_account, name='twitter_add_account'),
	url(r'^twitter/accounts/act/', views.twitter_act_account, name='twitter_act_account'),
	url(r'^twitter/accounts/deact/', views.twitter_deact_account, name='twitter_deact_account'),
	url(r'^twitter/accounts/view/', views.twitter_view_accounts, name='twitter_view_accounts'),
	url(r'^twitter/accounts/(?P<id>[0-9]+)/auto-post/', views.twitter_auto_post_account, name='twitter_auto_post_account'),
	url(r'^twitter/accounts/edit/(?P<id>[0-9]+)/', views.twitter_edit_account, name='twitter_edit_account'),
	url(r'^twitter/accounts/remove/(?P<id>[0-9]+)/', views.twitter_remove_account, name='twitter_remove_account'),
	
	##Twitter form urls
	url(r'^twitter/tweet/post/', views.twitter_post_tweet, name='twitter_post_tweet'),
	url(r'^twitter/accounts/auto-post/save-meta', views.twitter_auto_post_save_meta, name='twitter_auto_post_save_meta'),

	## Follow accounts
	url(r'^twitter/accounts/(?P<id>[0-9]+)/auto-follow/', views.twitter_auto_follow_account, name='twitter_auto_follow_account'),
	url(r'^twitter/accounts/auto-follow-add-account', views.twitter_auto_follow_account_add, name='twitter_auto_follow_account_add'),
	url(r'^twitter/accounts/auto-follow/', views.form_twitter_auto_follow_accounts, name='form_twitter_follow_accounts'),
	url(r'^twitter/accounts/auto-unfollow/', views.form_twitter_auto_unfollow_accounts, name='form_twitter_unfollow_accounts'),
	url(r'^twitter/accounts/add-phrase', views.form_twitter_follow_accounts_add_phrase, name='form_twitter_follow_accounts_add_phrase'),
	url(r'^twitter/accounts/auto-follow-save-meta', views.twitter_auto_follow_save_meta, name='twitter_auto_follow_save_meta'),
	url(r'^twitter/accounts/remove-phrase/(?P<twitter_account_id>[0-9]+)/(?P<phrase_id>[0-9]+)', views.twitter_remove_attached_phrase, name='twitter_remove_attached_phrase'),

	## Retweet accounts
	url(r'^twitter/accounts/(?P<id>[0-9]+)/auto-retweet/', views.twitter_auto_retweet_account, name='twitter_auto_retweet_account'),
	url(r'^twitter/accounts/auto-retweet-add-account', views.twitter_auto_retweet_account_add, name='twitter_auto_retweet_account_add'),
	url(r'^twitter/accounts/auto-retweet/', views.form_twitter_retweet_accounts, name='form_twitter_retweet_accounts'),
	url(r'^twitter/accounts/auto-retweet-save-meta', views.twitter_auto_retweet_save_meta, name='twitter_auto_retweet_save_meta'),

	## Auto direct messages
	url(r'^twitter/accounts/(?P<id>[0-9]+)/auto-direct-message/', views.twitter_auto_direct_message_account, name='twitter_auto_direct_message_account'),
	##url(r'^twitter/accounts/auto-direct-message/', views.twitter_direct_message_accounts, name='twitter_direct_message'),
	url(r'^twitter/accounts/auto-direct-message-save-message-template', views.twitter_direct_message_save_template, name='twitter_direct_message_save_template'),

	## Auto Twitter Analyzer
	url(r'^twitter/accounts/(?P<id>[0-9]+)/auto-twitter-analyzer/', views.twitter_auto_twitter_analyzer_account, name='twitter_auto_twitter_analyzer_account'),
	url(r'^twitter/accounts/auto-twitter-analyzer/add-account', views.twitter_auto_twitter_analyzer_account_add, name='twitter_auto_twitter_analyzer_account_add'),
	url(r'^twitter/accounts/auto-twitter-analyzer-save-meta/', views.form_twitter_auto_twitter_analyzer_save_meta, name='form_twitter_auto_twitter_analyzer_save_meta'),

	## Helper for removing attached accounts
	url(r'^twitter/attached-accounts/remove/(?P<twitter_user_id>[0-9]+)/(?P<id>[0-9]+)/(?P<for_attached_tool>[\w]+)/', views.twitter_remove_attached_account, name='twitter_remove_attached_account'),
]