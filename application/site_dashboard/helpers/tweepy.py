from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from site_dashboard.models import Tweet
from site_dashboard.models import TwitterUser
from site_dashboard.models import Follow
from site_dashboard.models import Retweet
import tweepy
import json

def fetch_tweets(user = None):
	if user == None:
		##Fetch tweets by all users
		tweets = Tweet.objects.all()
	else:
		tweets = Tweet.objects.filter(added_by_twitter_user_id = user)
	return tweets

def fetch_followers(user = None):
	if user == None:
		##Fetch tweets by all users
		follows = Follow.objects.all()
	else:
		follows = Follow.objects.filter(added_by_twitter_user_id = user)
	return follows

def fetch_retweets(user = None):
	if user == None:
		##Fetch tweets by all users
		retweets = Retweet.objects.all()
	else:
		retweets = Retweet.objects.filter(added_by_twitter_user_id = user)
	return retweets

def post_tweet(twitter_user_id, tweet_data):
	context = {
				'status': 0,
				'message': 'Invalid request!'
			}
	#Fetch access token and other info to post a tweet
	twitter_user = TwitterUser.objects.get(pk=twitter_user_id)
	consumer_key = twitter_user.twitter_consumer_key
	consumer_secret = twitter_user.twitter_consumer_secret
	access_token = twitter_user.twitter_access_token
	access_token_secret = twitter_user.twitter_access_token_secret
	try:
		auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
		auth.set_access_token(access_token, access_token_secret)
		api = tweepy.API(auth)
		api.update_status(tweet_data)
		return True
	except tweepy.TweepError as e:
		data = json.loads(e.reason.replace("[", "").replace("]", "").replace("'", "\""))
		error_code = data['code']
		error_message = data['message']
		return error_message


def follow_accounts(twitter_user, username, follow_accounts_count):
	context = {
				'status': 0,
				'message': 'Invalid request!'
			}
	#Fetch access token and other info to post a tweet
	consumer_key = twitter_user.twitter_consumer_key
	consumer_secret = twitter_user.twitter_consumer_secret
	access_token = twitter_user.twitter_access_token
	access_token_secret = twitter_user.twitter_access_token_secret
	try:
		auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
		auth.set_access_token(access_token, access_token_secret)
		api = tweepy.API(auth)
		followers = []
		for follower in tweepy.Cursor(api.friends).items():
			followers.append(follower)
		# for 

		# return ids

		
	except tweepy.TweepError as e:
		data = json.loads(e.reason.replace("[", "").replace("]", "").replace("'", "\""))
		error_code = data['code']
		error_message = data['message']
		return error_message

	return followers