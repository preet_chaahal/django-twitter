from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from site_dashboard.models import TwitterAttachedAccountsForFollow
from site_dashboard.models import TwitterAttachedAccountsForAnalyzer
from site_dashboard.models import TwitterAttachedAccountsForRetweet
from site_dashboard.models import TwitterUser
from site_dashboard.models import TwitterAutoFollowMeta
from time import sleep
import tweepy
import json

def post_tweet(twitter_user, tweet_data):
	context = {
				'status': 0,
				'message': 'Invalid request!'
			}
	#Fetch access token and other info to post a tweet
	consumer_key = twitter_user.twitter_consumer_key
	consumer_secret = twitter_user.twitter_consumer_secret
	access_token = twitter_user.twitter_access_token
	access_token_secret = twitter_user.twitter_access_token_secret
	try:
		auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
		auth.set_access_token(access_token, access_token_secret)
		api = tweepy.API(auth, wait_on_rate_limit=True)
		api.update_status(tweet_data['message'])
		return True
	except tweepy.TweepError as e:
		data = json.loads(e.reason.replace("[", "").replace("]", "").replace("'", "\""))
		error_code = data['code']
		error_message = data['message']
		return error_message

## Auto-direct message
def auto_direct_message(twitter_user_id, message):
	twitter_user = TwitterUser.objects.get(pk=twitter_user_id)
	consumer_key = twitter_user.twitter_consumer_key
	consumer_secret = twitter_user.twitter_consumer_secret
	access_token = twitter_user.twitter_access_token
	access_token_secret = twitter_user.twitter_access_token_secret
	message_sent_count = 0
	try:
		auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
		auth.set_access_token(access_token, access_token_secret)
		api = tweepy.API(auth, wait_on_rate_limit=True)
		me = api.me()
		outer_loop_counter=0		
		for follower in tweepy.Cursor(api.followers_ids).items():
			api.send_direct_message(follower, text=message)
			message_sent_count += 1
			
		returnData = {'status': True, 'message_sent_count': message_sent_count}

	except tweepy.TweepError as e:
		error_code = e.response
		error_message = e.response.text
		returnData = {'status': False, 'error_code': error_code, 'error_message': error_message}

	return returnData

## Auto-follow phrases helpers
def fetch_attached_phrases(twitterUserId = None):	
	if twitterUserId == None:
		##Fetch phrases by all users
		phrases = TwitterAutoFollowMeta.objects.all()
	else:
		phrases = TwitterAutoFollowMeta.objects.filter(added_by_twitter_user=twitterUserId)
	return phrases

## Auto-follow helpers
def fetch_attached_followers(twitterUserId = None):	
	if twitterUserId == None:
		##Fetch followers by all users
		followers = TwitterAttachedAccountsForFollow.objects.all()
	else:
		followers = TwitterAttachedAccountsForFollow.objects.filter(added_by_twitter_user=twitterUserId)
	return followers

def follow_accounts(twitter_user_id, twitter_ids_to_follow, follow_count_per_account=10):
	#Fetch access token and other info to post a tweet
	follow_count_per_account = int(follow_count_per_account)
	twitter_user = TwitterUser.objects.get(pk=twitter_user_id)
	consumer_key = twitter_user.twitter_consumer_key
	consumer_secret = twitter_user.twitter_consumer_secret
	access_token = twitter_user.twitter_access_token
	access_token_secret = twitter_user.twitter_access_token_secret
	try:
		auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
		auth.set_access_token(access_token, access_token_secret)
		api = tweepy.API(auth, wait_on_rate_limit_notify=True, wait_on_rate_limit=True)
		me = api.me()
		follow_count = 0
		outer_loop_counter=0
		##Getting list of followers
		my_friends_ids = []
		for friend in tweepy.Cursor(api.friends).items():
			my_friends_ids.append(friend.id)

		for twitter_id_to_follow in twitter_ids_to_follow:
			inner_loop_count = 0
			for follower in tweepy.Cursor(api.followers_ids, twitter_id_to_follow).items():
				if (inner_loop_count == follow_count_per_account):
					break
				if not follower in my_friends_ids:
					api.create_friendship(follower)
					follow_count += 1
					inner_loop_count += 1

		returnData = {'status': True, 'follow_count': follow_count}

	except tweepy.TweepError as e:
		error_code = e.response
		error_message = e.response.text
		returnData = {'status': False, 'error_code': error_code, 'error_message': error_message, 'follow_count': 0}

	return returnData

def unfollow_accounts(twitter_user_id, twitter_ids_to_unfollow=False, unfollow_count_per_account=10):
	#Fetch access token and other info to post a tweet
	unfollow_count_per_account = int(unfollow_count_per_account)
	twitter_user = TwitterUser.objects.get(pk=twitter_user_id)
	consumer_key = twitter_user.twitter_consumer_key
	consumer_secret = twitter_user.twitter_consumer_secret
	access_token = twitter_user.twitter_access_token
	access_token_secret = twitter_user.twitter_access_token_secret
	unfollow_count = 0
	try:
		auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
		auth.set_access_token(access_token, access_token_secret)
		api = tweepy.API(auth, wait_on_rate_limit=True)
		me = api.me()
		outer_loop_counter=0		
		for friend in tweepy.Cursor(api.friends_ids).items():
			api.destroy_friendship(friend)
			unfollow_count += 1
			
		returnData = {'status': True, 'unfollow_count': unfollow_count}

	except tweepy.TweepError as e:
		error_code = e.response
		error_message = e.response.text
		returnData = {'status': False, 'error_code': error_code, 'error_message': error_message}

	return returnData


## Auto-retweet accounts
def retweet_accounts(twitter_user_id, twitter_ids_to_retweet, allow_duplicate_retweet=True):
	#Fetch access token and other info to post a tweet
	twitter_user = TwitterUser.objects.get(pk=twitter_user_id)
	consumer_key = twitter_user.twitter_consumer_key
	consumer_secret = twitter_user.twitter_consumer_secret
	access_token = twitter_user.twitter_access_token
	access_token_secret = twitter_user.twitter_access_token_secret

	retweet_count = 0
	retweet_done = False		
	status = False
	error_message = False
	error_code = False
	try:
		auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
		auth.set_access_token(access_token, access_token_secret)
		api = tweepy.API(auth, wait_on_rate_limit=True)
		me = api.me()
		for twitter_id_to_retweet in twitter_ids_to_retweet:
			
			twitter_account_status_objects = api.user_timeline(id=twitter_id_to_retweet)
			loopcounter = 0
			for twitter_account_status_object in twitter_account_status_objects:
				if retweet_done:
					break
				if allow_duplicate_retweet or (twitter_account_status_object.retweet_count  == 0) or (loopcounter == len(twitter_account_status_objects)):
					retweet_done = True
					## Perform Retweet
					api.retweet(twitter_account_status_object.id)
					status = True
					retweet_count += 1#twitter_account_status_object.retweet_count 
				loopcounter += 1

	except tweepy.TweepError as e:
		error_code = e.response
		error_message = e.response.text
		
	return {'status': status, 'retweet_count': retweet_count, 'error_code': error_code, 'error_message': error_message}
	

def fetch_attached_retweet_accounts(twitterUserId = None):	
	if twitterUserId == None:
		##Fetch retweet_accounts by all users
		retweet_accounts = TwitterAttachedAccountsForRetweet.objects.all()
	else:
		retweet_accounts = TwitterAttachedAccountsForRetweet.objects.filter(added_by_twitter_user=twitterUserId)
	return retweet_accounts


def fetch_attached_twitter_analyzer_accounts(twitterUserId = None):	
	if twitterUserId == None:
		##Fetch followers by all users
		accounts = TwitterAttachedAccountsForAnalyzer.objects.all()
	else:
		accounts = TwitterAttachedAccountsForAnalyzer.objects.filter(added_by_twitter_user=twitterUserId)
	return accounts