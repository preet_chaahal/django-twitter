from django.urls import reverse
from django.shortcuts import render
from django.http import JsonResponse
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from site_dashboard.models import TwitterUser
from .forms import AddTwitterAccountForm
from .forms import UpdateTwitterAccountForm
from .forms import AddTweetForm
from .forms import AddAttachedTwitterAccountForFollowForm
from .forms import AddAttachedTwitterAccountForRetweetForm
from .forms import FollowTwitterAccountForm
from .forms import RetweetTwitterAccountForm
from .forms import UpdateDirectMessageTemplateForm
from .forms import AddTwitterAutoPostMetaForm
from .forms import AddTwitterAutoFollowMetaForm
from .forms import AddAttachedTwitterAccountForAnalyzerForm
from site_dashboard.helpers import tweepy as tweepy_helper
from site_dashboard.helpers import twitter_followers as twitter_followers_helper
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django_celery_beat.models import PeriodicTask, IntervalSchedule
import tweepy
import time
import json
from .models import TwitterAutoPostMeta
from .models import TwitterAutoFollowMeta
from .models import TwitterAttachedAccountsForRetweet
from .models import TwitterUserStats
from .models import TwitterAttachedAccountsForFollow
from .models import TwitterAttachedAccountsForAnalyzer, TwitterAutoDirectMessageMeta
from django.shortcuts import get_object_or_404
from django.conf import settings
from datetime import datetime
from django_celery_results.models import TaskResult
from django.core.files.storage import FileSystemStorage
from django.utils.crypto import get_random_string
from site_dashboard.models import ToolStats 

def upload_client_secret_file(request):
    filename_to_save = False
    if request.FILES.get('file'):
        myfile = request.FILES.get('file')
        filename_to_save = get_random_string(32) + '.json'
        fs = FileSystemStorage()
        filename = fs.save(filename_to_save, myfile)
        uploaded_file_url = fs.url(filename)
        
    return filename_to_save

def delete_file(file):
	try:
		fs = FileSystemStorage()
		fs.delete(file)
		return True
	except Exception as e:
		return False


##Pages

@login_required
def index(request):
	current_month = datetime.now().month
	context = {
		'task_auto_follow': PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_FOLLOW),
		'task_auto_retweet': PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_RETWEET),
		'task_auto_direct_message': PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_DIRECT_MESSAGE),
		##'task_auto_post': PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_POST),
		'task_auto_twitter_analyzer': PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_TWITTER_ANALYZER),

		## Twitter accounst added for stats
		'twitter_accounts_count': TwitterUser.objects.filter(is_active=True).count(),
		'twitter_accounts_added_this_month_count': TwitterUser.objects.filter(is_active=True, created_at__month=current_month).count(),
		'twitter_accounts_added_this_month': TwitterUser.objects.filter(is_active=True, created_at__month=current_month).all(),
		'cron_jobs_feed': TaskResult.objects.all()[:20],
	}
	return render(request, 'site_dashboard/index.html', context)

@login_required
def error_reporting(request):
	current_month = datetime.now().month
	context = {
		'cron_jobs_feed': TaskResult.objects.all()[:20],
	}
	return render(request, 'site_dashboard/error_reporting.html', context)

def login(request):
	if request.method == 'POST':
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(username=username, password=password)
		if user is not None:
			auth_login(request, user)
			return redirect('home')
		else:
			messages.add_message(request, messages.ERROR, 'Invalid login, please check your credentials')
			return render(request, 'site_dashboard/login.html')
	
	return render(request, 'site_dashboard/login.html')

@login_required
def logout(request):
	auth_logout(request)
	return redirect('login')

def tweet(request):
	consumer_key = 'EFxP2eJuG5Pk2YnTtAayJHO8c'
	consumer_secret = 'aMwJboeqFAr5WWjZktl94IzS16b9EqrsJGgFhwPeizdfepl9Qc'
	access_token = '308534326-aLKanOJGpJUhRZcEKYX1mKsrgTKFDPMLiZFk8YGm'
	access_token_secret = '9iguRXoqwGdgXkRld2u8tiGfM2PHxXbb1GyC4AqyrOgLB'
	auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
	auth.set_access_token(access_token, access_token_secret)
	api = tweepy.API(auth)

	data = api.me()
	return HttpResponse(data)

#Twitter related methods
def twitter_add_account(request):
	if request.method == 'POST':
		form = AddTwitterAccountForm(request.POST)
		if form.is_valid():
			t_user = form.save(commit=False)
			if request.POST.get('is_active') == '1':
				t_user.is_active = True
			else:
				t_user.is_active = False
			t_user.config = {'auto_post': True, 'auto_follow': True, 'auto_retweet': True, 'auto_direct_message': True, 'auto_twitter_analyzer': True, 'auto_follow_phrase': True, 'auto_unfollow': True}
			t_user.save()
			t_user.user.add(request.POST['user_id'])
			##Adding basic meta info to the twitter account added
			
			context = {
				'status': 1,
				'message': "Added twitter account successfully!"
			}
			messages.add_message(request, messages.SUCCESS, 'Twitter account updated!')
			return HttpResponseRedirect(reverse('twitter_view_accounts'))
		else:
			messages.add_message(request, messages.ERROR, 'Invalid form data!')

	context = {
		'users': User.objects.all(),
		'addTwitterAccountForm': AddTwitterAccountForm()
	}
	return render(request, "site_dashboard/twitter/add-account.html", context)

#Twitter related methods
def twitter_act_account(request):
	if request.method == 'POST':
		if request.POST.get('user_id'):
			twitter_user = TwitterUser.objects.get(pk=request.POST.get('user_id'))
			twitter_user.is_active = True
			twitter_user.save()
			##Adding basic meta info to the twitter account added
			context = {
				'status': 1,
				'message': "Twitter Account activated successfully!"
			}
			# messages.add_message(request, messages.SUCCESS, 'Twitter Account de-activated successfully!')
			return JsonResponse(context);
		else:
			return JsonResponse({'status': 0, 'message': 'Something went wrong trying to activate user account'});

	return JsonResponse({'status': -1, 'message': 'Invalid request!'});

#Twitter related methods
def twitter_deact_account(request):
	if request.method == 'POST':
		if request.POST.get('user_id'):
			twitter_user = TwitterUser.objects.get(pk=request.POST.get('user_id'))
			twitter_user.is_active = False
			twitter_user.save()
			##Adding basic meta info to the twitter account added
			context = {
				'status': 1,
				'message': "Twitter Account de-activated successfully!"
			}
			messages.add_message(request, messages.SUCCESS, 'Twitter Account de-activated successfully!')
			return JsonResponse(context);
		else:
			return JsonResponse({'status': 0, 'message': 'Something went wrong trying to de-activate user account'});

	return JsonResponse({'status': -1, 'message': 'Invalid request!'});

def twitter_view_accounts(request):
	context = {
		'twitter_users': TwitterUser.objects.filter(user=request.user).all()
	}
	return render(request, "site_dashboard/twitter/view-accounts.html", context)

#Individual twitter account view
def twitter_auto_post_account(request, id):
	tweets = tweepy_helper.fetch_tweets(id)[:5]
	try:
		current_twitter_user_meta = TwitterAutoPostMeta.objects.get(added_by_twitter_user_id=id)
	except Exception as e:
		current_twitter_user_meta = None
	
	stats = ToolStats.objects.filter(key='auto_post')
	total_micorseconds = 0
	for stat in stats:
		time = stat.end_time - stat.created_at
		total_micorseconds += time.microseconds
	runtime_days = int(total_micorseconds / (60*60*24*1000))
	runtime_hours = int(total_micorseconds / (60*60*1000))
	runtime_minutes = int(total_micorseconds / (60*1000))

	context = {
		'twitter_users': TwitterUser.objects.all(),
		'current_twitter_user': TwitterUser.objects.get(id = id),
		'current_twitter_user_meta': current_twitter_user_meta,
		'tweets': tweets,
		'runtime_days': runtime_days,
		'runtime_hours': runtime_hours,
		'runtime_minutes': runtime_minutes
	}
	return render(request, "site_dashboard/twitter/auto-post-account.html", context)

def twitter_auto_post_save_meta(request):
	if request.method == 'POST':
		##Check if client secrets already exist
		flag = True
		old_client_secret_file = False
		try:
			existing_obj = TwitterAutoPostMeta.objects.get(added_by_twitter_user_id=request.POST['added_by_twitter_user'])
			old_client_secret_file = existing_obj.client_secret
		except Exception as e:
			flag = False
		if not flag:
			form = AddTwitterAutoPostMetaForm(request.POST)
		else:
			form = AddTwitterAutoPostMetaForm(request.POST, instance = existing_obj)	

		if form.is_valid():
			form_extended = form.save(commit=False)
			new_client_secret = upload_client_secret_file(request)
			if new_client_secret:
				form_extended.client_secret = new_client_secret
			if old_client_secret_file: 
				delete_file(old_client_secret_file)
			
			twitter_user = TwitterUser.objects.get(pk=request.POST.get('added_by_twitter_user'))
			if request.POST.get('tool_status'):
				form_extended.is_active = True
				twitter_user.config['auto_post'] = True
			else:
				form_extended.is_active = False
				twitter_user.config['auto_post'] = False
			
			twitter_user.save()
			form_extended.save()
			messages.add_message(request, messages.SUCCESS, 'Twitter account data for auto-post updated!')
		else:
			messages.add_message(request, messages.ERROR, 'Something went wrong trying to attach client screts for google sheets!')

	return HttpResponseRedirect(reverse("twitter_auto_post_account", args=(), kwargs={'id':request.POST['added_by_twitter_user']}))

#Individual twitter auto follow accounts view
def twitter_auto_follow_account(request, id):
	follows = tweepy_helper.fetch_followers(id)
	follow_backs_count = 0
	
	for follow in follows:
		if follow.follow_response == 'F':
			follow_backs_count += 1

	stats = ToolStats.objects.filter(key='auto_follow')
	total_micorseconds = 0
	for stat in stats:
		time = stat.end_time - stat.created_at
		total_micorseconds += time.microseconds
	runtime_days = int(total_micorseconds / (60*60*24*1000))
	runtime_hours = int(total_micorseconds / (60*60*1000))
	runtime_minutes = int(total_micorseconds / (60*1000))

	auto_follow_attached_accounts = twitter_followers_helper.fetch_attached_followers(id)
	auto_follow_attached_phrases = twitter_followers_helper.fetch_attached_phrases(id)
	context = {
		'twitter_users': TwitterUser.objects.all(),
		'current_twitter_user': TwitterUser.objects.get(id = id),
		'attached_followers': auto_follow_attached_accounts,
		'addAttachedTwitterAccountForFollowForm': AddAttachedTwitterAccountForFollowForm,
		'follows': follows,
		'phrases': auto_follow_attached_phrases,
		'follow_backs_count': follow_backs_count,
		'runtime_days': runtime_days,
		'runtime_hours': runtime_hours,
		'runtime_minutes': runtime_minutes
	}
	return render(request, "site_dashboard/twitter/auto-follow-account.html", context)

def twitter_auto_follow_save_meta(request):
	if request.method == 'POST':
		##Check if client secrets already exist
		twitter_user = TwitterUser.objects.get(pk=request.POST.get('added_by_twitter_user'))
			
		if request.POST.get('tool_status'):
			twitter_user.config['auto_follow'] = True
		else:
			twitter_user.config['auto_follow'] = False

		if request.POST.get('tool_status_auto_unfollow'):
			twitter_user.config['auto_unfollow'] = True
		else:
			twitter_user.config['auto_unfollow'] = False

		if request.POST.get('tool_status_auto_follow_phrase'):
			twitter_user.config['auto_follow_phrase'] = True
		else:
			twitter_user.config['auto_follow_phrase'] = False

		flag = twitter_user.save()

		if flag:
			messages.add_message(request, messages.ERROR, 'Something went wrong trying to update tool data!')
		else:
			messages.add_message(request, messages.SUCCESS, 'Twitter tool updated!')

	return HttpResponseRedirect(reverse("twitter_auto_follow_account", args=(), kwargs={'id':request.POST.get('added_by_twitter_user')}))

#Individual twitter account view
def twitter_auto_follow_account_add(request):
	if request.method == 'POST':
		form = AddAttachedTwitterAccountForFollowForm(request.POST)
		if form.is_valid():
			form.save()
			messages.add_message(request, messages.SUCCESS, 'Twitter account attached to list of auto-followers!')
		else:
			messages.add_message(request, messages.SUCCESS, 'Something went wrong trying to attach twitter account with list of auto-followers!')

	return HttpResponseRedirect(reverse("twitter_auto_follow_account", args=(), kwargs={'id':request.POST['added_by_twitter_user']}))

#Auto Follow Accounts Phrase
def form_twitter_follow_accounts_add_phrase(request):
	if request.method == 'POST':
		form = AddTwitterAutoFollowMetaForm(request.POST)
		if form.is_valid():
			form.save()
			messages.add_message(request, messages.SUCCESS, 'Phrase attached to twitter account!')
		else:
			messages.add_message(request, messages.ERROR, 'Something went wrong trying to attach phrase with twitter account!')

	return HttpResponseRedirect(reverse("twitter_auto_follow_account", args=(), kwargs={'id':request.POST['added_by_twitter_user']}))

#Auto Follow Accounts Phrase remove
def twitter_remove_attached_phrase(request, twitter_account_id, phrase_id):
	if phrase_id:
		flag = TwitterAutoFollowMeta.objects.get(pk=phrase_id).delete()
		if flag:
			messages.add_message(request, messages.SUCCESS, 'Phrase removed from twitter account!')
		else:
			messages.add_message(request, messages.ERROR, 'Something went wrong trying to remove phrase from twitter account!')

	return HttpResponseRedirect(reverse("twitter_auto_follow_account", args=(), kwargs={'id':twitter_account_id}))

#Form handler for auto-follow accounts
def form_twitter_auto_follow_accounts(request):
	if request.method == 'POST':
		form = FollowTwitterAccountForm(request.POST)
		added_by_twitter_user = request.POST['added_by_twitter_user']
		twitter_follow_account_id = request.POST['twitter_follow_account_id']
		follow_count_per_account = request.POST['follow_count_per_account']
		if form.is_valid():
			result = twitter_followers_helper.follow_accounts(added_by_twitter_user, twitter_follow_account_id, follow_count_per_account)
			if result['status']:
				form = form.save(commit=False)
				form.twitter_follow_phrase = ''
				form.follow_response = 'U'
				form.follow_status = 'U'
				form.save()
				message = str(result['follow_count']) + ' accounts followed!'
				messages.add_message(request, messages.SUCCESS, message)
			else:
				message = 'Something went wrong! Error code: ' + result['error_message']
				messages.add_message(request, messages.ERROR, message)
		else:
			return HttpResponse("Invalid request!")

	return HttpResponseRedirect(reverse("twitter_auto_follow_account", args=(), kwargs={'id':request.POST['added_by_twitter_user']}))

#Form handler for auto-unfollow accounts
def form_twitter_auto_unfollow_accounts(request):
	if request.method == 'POST':
		##Twitter handler associated with this request
		added_by_twitter_user = request.POST['added_by_twitter_user']
		twitter_unfollow_account_id = True
		unfollow_count_per_account = 100
		result = twitter_followers_helper.unfollow_accounts(added_by_twitter_user, twitter_unfollow_account_id, unfollow_count_per_account)
		if result['status']:
			# form = form.save(commit=False)
			# form.twitter_follow_phrase = ''
			# form.follow_response = 'U'
			# form.follow_status = 'U'
			# form.save()
			message = str(result['unfollow_count']) + ' accounts unfollowed!'
			messages.add_message(request, messages.SUCCESS, message)
		else:
			message = 'Something went wrong! Error code: ' + result['error_message']
			messages.add_message(request, messages.ERROR, message)

	return HttpResponseRedirect(reverse("twitter_auto_follow_account", args=(), kwargs={'id':request.POST['added_by_twitter_user']}))


#Individual twitter account edit
def twitter_edit_account(request, id):
	if request.method == 'POST':
		existing_obj = TwitterUser.objects.get(pk=request.POST['id'])
		form = UpdateTwitterAccountForm(request.POST, instance = existing_obj)
		if form.is_valid():
			t_user = form.save(commit=False)
			if request.POST.get('is_active') == '1':
				t_user.is_active = True
			else:
				t_user.is_active = False
			t_user.save()
			# t_user.user.delete(id=request.POST['user_id'])
			messages.add_message(request, messages.SUCCESS, 'Updated twitter account successfully!')
			return HttpResponseRedirect(reverse('twitter_view_accounts'))
		else:
			messages.add_message(request, messages.DANGER, 'Something went wrong trying to update the requested twitter account!')

	context = {
		'users': User.objects.all(),
		'addTwitterAccountForm': AddTwitterAccountForm(),
		'current_twitter_user': TwitterUser.objects.get(id = id),
	}
	return render(request, "site_dashboard/twitter/edit-account.html", context)

#Individual twitter account remove
def twitter_remove_account(request, id):
	flag = TwitterUser.objects.get(id = id).delete()
	if flag:
		messages.add_message(request, messages.WARNING, 'Twitter account removed!')
	else:
		messages.add_message(request, messages.DANGER, 'Something went wrong trying to remove twitter account!')
	return HttpResponseRedirect(reverse('twitter_view_accounts'))

#Individual twitter account remove
def twitter_remove_attached_account(request, twitter_user_id, id, for_attached_tool=None):
	if for_attached_tool is not None: 
		if for_attached_tool == 'auto_retweet':
			flag = TwitterAttachedAccountsForRetweet.objects.get(id = id, added_by_twitter_user_id=twitter_user_id).delete()
			if flag:
				messages.add_message(request, messages.WARNING, 'Attached twitter account removed!')
			else:
				messages.add_message(request, messages.DANGER, 'Something went wrong trying to remove the attached twitter account!')
			return HttpResponseRedirect(reverse('twitter_auto_retweet_account', args=(), kwargs={'id':twitter_user_id}))

		if for_attached_tool == 'auto_follow':
			flag = TwitterAttachedAccountsForFollow.objects.get(id = id, added_by_twitter_user_id=twitter_user_id).delete()
			if flag:
				messages.add_message(request, messages.WARNING, 'Attached twitter account removed!')
			else:
				messages.add_message(request, messages.DANGER, 'Something went wrong trying to remove the attached twitter account!')
			return HttpResponseRedirect(reverse('twitter_auto_follow_account', args=(), kwargs={'id':twitter_user_id}))	

		if for_attached_tool == 'auto_twitter_analyzer':
			flag = TwitterAttachedAccountsForAnalyzer.objects.get(id = id, added_by_twitter_user_id=twitter_user_id).delete()
			if flag:
				messages.add_message(request, messages.WARNING, 'Attached twitter account removed!')
			else:
				messages.add_message(request, messages.DANGER, 'Something went wrong trying to remove the attached twitter account!')
			return HttpResponseRedirect(reverse('twitter_auto_twitter_analyzer_account', args=(), kwargs={'id':twitter_user_id}))	
	
	return HttpResponse("Invalid request!")

#Twitter form related nethods
def twitter_post_tweet(request):
	context = {
				'status': 0,
				'message': 'Invalid request!'
			}

	if request.method == 'POST':
		form = AddTweetForm(request.POST)
		if form.is_valid():
			tweet_data = form.save(commit=False)
			tweet_data.status = True

			message = request.POST['message']
			added_by_twitter_user = request.POST['added_by_twitter_user']

			flag = tweepy_helper.post_tweet(added_by_twitter_user, message)
			if flag == True:
				##save tweet to database
				tweet_data.save()
				context = {
					'status': 1,
					'message': "Added tweet!"
				}
				messages.add_message(request, messages.SUCCESS, 'Status tweeted successfully!')
			else:
				context = {
					'status': 1,
					'message': 'Tweet posted!'
				}
				messages.add_message(request, messages.SUCCESS, 'Something went wrong trying to tweet!')

	return HttpResponseRedirect(reverse("twitter_auto_post_account", args=(), kwargs={'id':request.POST['added_by_twitter_user']}))

def twitter_follow_accounts(request):
	context = {
				'status': 0,
				'message': 'Invalid request!'
			}

	if request.method == 'POST':
		form = FollowTwitterAccountForm(request.POST)
		if form.is_valid():
			tweet_data = form.save(commit=False)
			tweet_data.status = 1
			tweet_data.posted_by_user_id = request.user
			# data = {
			# 	'message': request.POST['message']
			# }
			users = tweepy_helper.follow_accounts(request.POST['added_by_twitter_account_id'], request.POST['twitter_username_to_follow'], 4)
			messages.add_message(request, messages.INFO, users)
			# return users;
			# for user in users
			if flag['status'] == True:
				##save tweet to database
				tweet_data.save()
				# context = {
				# 	'status': 1,
				# 	'message': "Added tweet!"
				# }
			# else:
			# 	context = {
			# 		'status': 1,
			# 		'message': 'Tweet posted!'
			# 	}

	return redirect('twitter_auto_follow_account', id = request.POST['request_by_twitter_account_id']);

def form_twitter_auto_follow_accounts_update_phrase(request):
	return TwitterUser.objects.filter(pk=request.POST['added_by_twitter_user']).update(follow_phrase=request.POST['twitter_follow_phrase'])

#Individual twitter account view
#Individual twitter account view
def twitter_auto_retweet_account_add(request):
	if request.method == 'POST':
		form = AddAttachedTwitterAccountForRetweetForm(request.POST)
		if form.is_valid():
			form.save()
			messages.add_message(request, messages.SUCCESS, 'Twitter account added to the list of auto-retweet accounts!')
		else:
			messages.add_message(request, messages.ERROR, 'Something went wrong trying to attach twitter account with list of auto-retweet accounts!')

	return HttpResponseRedirect(reverse("twitter_auto_retweet_account", args=(), kwargs={'id':request.POST['added_by_twitter_user']}))

def twitter_auto_retweet_account(request, id):
	auto_retweet_attached_accounts =  twitter_followers_helper.fetch_attached_retweet_accounts(id)
	retweets = tweepy_helper.fetch_retweets(id)
	stats = ToolStats.objects.filter(key='auto_retweet')
	total_micorseconds = 0
	for stat in stats:
		time = stat.end_time - stat.created_at
		total_micorseconds += time.microseconds
	runtime_days = int(total_micorseconds / (60*60*24*1000))
	runtime_hours = int(total_micorseconds / (60*60*1000))
	runtime_minutes = int(total_micorseconds / (60*1000))

	context = {
		'twitter_users': TwitterUser.objects.all(),
		'current_twitter_user': TwitterUser.objects.get(id = id),
		'attached_retweet_accounts': auto_retweet_attached_accounts,
		'addAttachedTwitterAccountForRetweetForm': AddAttachedTwitterAccountForRetweetForm,
		'retweets': retweets,
		'runtime_days': runtime_days,
		'runtime_hours': runtime_hours,
		'runtime_minutes': runtime_minutes
	}
	
	return render(request, "site_dashboard/twitter/auto-retweet-account.html", context)

#Form handler for auto-follow accounts
def form_twitter_retweet_accounts(request):
	allow_duplicate_retweet = False
	if request.method == 'POST':
		form = RetweetTwitterAccountForm(request.POST)
		added_by_twitter_user = request.POST['added_by_twitter_user']
		twitter_retweet_account_id = request.POST.getlist('twitter_retweet_account_id')
		follow_count_per_account = allow_duplicate_retweet
		if form.is_valid():
			result = twitter_followers_helper.retweet_accounts(added_by_twitter_user, twitter_retweet_account_id, allow_duplicate_retweet)
			if result['status']:
				form = form.save(commit=False)
				form.twitter_follow_phrase = ''
				form.follow_response = 'U'
				form.follow_status = 'U'
				form.save()
				message = str(result['retweet_count']) + ' retweets successful!'
				messages.add_message(request, messages.SUCCESS, message)
			else:
				message = 'Something went wrong! Error code: ' + result['error_message']
				messages.add_message(request, messages.ERROR, result['error_message'])
		else:
			return HttpResponse("Invalid request!")

	return HttpResponseRedirect(reverse("twitter_auto_retweet_account", args=(), kwargs={'id':request.POST['added_by_twitter_user']}))

def twitter_auto_retweet_save_meta(request):
	if request.method == 'POST':
		##Check if client secrets already exist
		twitter_user = TwitterUser.objects.get(pk=request.POST.get('added_by_twitter_user'))
			
		if request.POST.get('tool_status'):
			twitter_user.config['auto_retweet'] = True
		else:
			twitter_user.config['auto_retweet'] = False

		flag = twitter_user.save()
		if flag:
			messages.add_message(request, messages.ERROR, 'Something went wrong trying to update tool data!')
		else:
			messages.add_message(request, messages.SUCCESS, 'Twitter tool updated!')

	return HttpResponseRedirect(reverse("twitter_auto_retweet_account", args=(), kwargs={'id':request.POST.get('added_by_twitter_user')}))

#Auto direct message account view
def twitter_auto_direct_message_account(request, id):
	twitter_user_meta, cretated = TwitterAutoDirectMessageMeta.objects.get_or_create(added_by_twitter_user_id = id)
	tweets = tweepy_helper.fetch_tweets(id)[:5]
	
	stats = ToolStats.objects.filter(key='auto_retweet')
	total_micorseconds = 0
	for stat in stats:
		time = stat.end_time - stat.created_at
		total_micorseconds += time.microseconds
	runtime_days = int(total_micorseconds / (60*60*24*1000))
	runtime_hours = int(total_micorseconds / (60*60*1000))
	runtime_minutes = int(total_micorseconds / (60*1000))

	context = {
		'twitter_users': TwitterUser.objects.all(),
		'current_twitter_user': TwitterUser.objects.get(id = id),
		'twitter_user_meta': twitter_user_meta,
		'tweets': tweets,
		'runtime_days': runtime_days,
		'runtime_hours': runtime_hours,
		'runtime_minutes': runtime_minutes
	}
	return render(request, "site_dashboard/twitter/auto-direct-message-account.html", context)

def form_twitter_auto_twitter_analyzer_save_meta(request):
	if request.method == 'POST':
		##Check if client secrets already exist
		twitter_user = TwitterUser.objects.get(pk=request.POST.get('added_by_twitter_user'))
			
		if request.POST.get('tool_status'):
			twitter_user.config['auto_twitter_analyzer'] = True
		else:
			twitter_user.config['auto_twitter_analyzer'] = False

		flag = twitter_user.save()

		if flag:
			messages.add_message(request, messages.ERROR, 'Something went wrong trying to update tool data!')
		else:
			messages.add_message(request, messages.SUCCESS, 'Twitter tool updated!')

	return HttpResponseRedirect(reverse("twitter_auto_twitter_analyzer_account", args=(), kwargs={'id':request.POST.get('added_by_twitter_user')}))

# Auto direct message
# def twitter_direct_message_accounts(request):
# 	context = {
# 				'status': 0,
# 				'message': 'Invalid request!'
# 			}

# 	if request.method == 'POST':
# 		form = AddDirectMessageForm(request.POST)
# 		if form.is_valid():
# 			tweet_data = form.save(commit=False)
# 			tweet_data.is_active = True
# 			result = twitter_followers_helper.auto_direct_message(request.POST['added_by_twitter_user'], request.POST['message'])
# 			if result['status']:
# 				form.save()
# 				message = str(result['message_sent_count']) + ' accounts sent direct message!'
# 				messages.add_message(request, messages.SUCCESS, message)
# 			else:
# 				message = 'Something went wrong! Error code: ' + result['error_message']
# 				messages.add_message(request, messages.ERROR, message)

# 	return HttpResponseRedirect(reverse("twitter_auto_direct_message_account", args=(), kwargs={'id':request.POST['added_by_twitter_user']}))

def twitter_direct_message_save_template(request):
	context = {
				'status': 0,
				'message': 'Invalid request!'
			}

	if request.method == 'POST':
		try:
			twitter_user, created = TwitterAutoDirectMessageMeta.objects.get_or_create(added_by_twitter_user_id=request.POST.get('added_by_twitter_user'))
		except:
			twitter_user = TwitterAutoDirectMessageMeta.objects.get(added_by_twitter_user_id=request.POST.get('added_by_twitter_user'))

		form = UpdateDirectMessageTemplateForm(request.POST or None, instance=twitter_user)
		if form.is_valid():
			if form.save():
				current_twitter_user = TwitterUser.objects.get(pk=request.POST.get('added_by_twitter_user'))
				if request.POST.get('tool_status'):
					current_twitter_user.config['auto_direct_message'] = True
				else:
					current_twitter_user.config['auto_direct_message'] = False
				current_twitter_user.save()
				messages.add_message(request, messages.SUCCESS, 'Auto response message updated successfully!')
			else:
				messages.add_message(request, messages.ERROR, 'Something went wrong trying to update message for auto-direct message!')

	return HttpResponseRedirect(reverse("twitter_auto_direct_message_account", args=(), kwargs={'id':request.POST['added_by_twitter_user']}))

# Auto Twitter Analyzer

def twitter_auto_twitter_analyzer_account(request, id):

	id_for_stats = id
	try:
		if request.GET.get('attached_account_id'):
			id_for_stats = request.GET['attached_account_id']
	except:
		id_for_stats = id
			
	auto_twitter_analyzer_accounts = twitter_followers_helper.fetch_attached_twitter_analyzer_accounts(id)

	try:
		twitter_user_accounts_stats = TwitterUserStats.objects.filter( added_by_twitter_user_id = id_for_stats ).order_by('-created_at').all()[:1]
		twitter_user_accounts_stats = twitter_user_accounts_stats[0]
		
	except Exception as den:
		twitter_user_accounts_stats = False
	
	context = {
		'twitter_users': TwitterUser.objects.all(),
		'current_twitter_user': TwitterUser.objects.get(id = id),
		'auto_twitter_analyzer_accounts': auto_twitter_analyzer_accounts,
		'twitter_user_accounts_stats': twitter_user_accounts_stats
	}
	
	return render(request, "site_dashboard/twitter/auto-twitter-analyzer-account.html", context)

def twitter_auto_twitter_analyzer_account_add(request):
	if request.method == 'POST':
		form = AddAttachedTwitterAccountForAnalyzerForm(request.POST)
		if form.is_valid():
			form.save()
			messages.add_message(request, messages.SUCCESS, 'Twitter account attached to list of accounts for auto-twitter analysis!')
		else:
			messages.add_message(request, messages.SUCCESS, 'Something went wrong!')

	return HttpResponseRedirect(reverse("twitter_auto_twitter_analyzer_account", args=(), kwargs={'id':request.POST['added_by_twitter_user']}))

# #Form handler for auto-follow accounts
# def form_twitter_retweet_accounts(request):
# 	allow_duplicate_retweet = False
# 	if request.method == 'POST':
# 		form = RetweetTwitterAccountForm(request.POST)
# 		added_by_twitter_user = request.POST['added_by_twitter_user']
# 		twitter_retweet_account_id = request.POST.getlist('twitter_retweet_account_id')
# 		follow_count_per_account = allow_duplicate_retweet
# 		if form.is_valid():
# 			result = twitter_followers_helper.retweet_accounts(added_by_twitter_user, twitter_retweet_account_id, allow_duplicate_retweet)
# 			if result['status']:
# 				form = form.save(commit=False)
# 				form.twitter_follow_phrase = ''
# 				form.follow_response = 'U'
# 				form.follow_status = 'U'
# 				form.save()
# 				message = str(result['retweet_count']) + ' retweets successful!'
# 				messages.add_message(request, messages.SUCCESS, message)
# 			else:
# 				message = 'Something went wrong! Error code: ' + result['error_message']
# 				messages.add_message(request, messages.ERROR, result['error_message'])
# 		else:
# 			return HttpResponse("Invalid request!")

# 	return HttpResponseRedirect(reverse("twitter_auto_retweet_account", args=(), kwargs={'id':request.POST['added_by_twitter_user']}))
