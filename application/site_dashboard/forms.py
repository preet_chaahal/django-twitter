from django import forms
from .models import TwitterUser
from .models import TwitterAttachedAccountsForFollow
from .models import TwitterAttachedAccountsForRetweet
from .models import TwitterAutoFollowMeta
from .models import Tweet
from .models import DirectMessage
from .models import Retweet
from .models import Follow
from .models import TwitterAutoPostMeta
from .models import TwitterAttachedAccountsForAnalyzer, TwitterAutoDirectMessageMeta

class AddTwitterAccountForm(forms.ModelForm):
	class Meta:
		model = TwitterUser
		fields = ('twitter_username', 'twitter_consumer_key', 'twitter_consumer_secret', 'twitter_access_token', 'twitter_access_token_secret', 'is_active', )

class UpdateTwitterAccountForm(forms.ModelForm):
	class Meta:
		model = TwitterUser
		fields = ('twitter_username', 'twitter_consumer_key', 'twitter_consumer_secret', 'twitter_access_token', 'twitter_access_token_secret', 'is_active', )

## Post a tweet form
class AddTweetForm(forms.ModelForm):
	class Meta:
		model = Tweet
		fields = ('added_by_twitter_user', 'message', )

class AddTwitterAutoPostMetaForm(forms.ModelForm):
	class Meta:
		model = TwitterAutoPostMeta
		fields = ('added_by_twitter_user', 'google_sheets_file_name', 'client_secret', 'is_active' )

## Post a tweet form
## This form was for manula form for sending auto DM
# class AddDirectMessageForm(forms.ModelForm):
# 	class Meta:
# 		model = DirectMessage
# 		fields = ('added_by_twitter_user', 'message', )

class UpdateDirectMessageTemplateForm(forms.ModelForm):
	class Meta:
		model = TwitterAutoDirectMessageMeta
		fields = ('twitter_auto_response_message', 'auto_delete_direct_messages_older_than_days', 'added_by_twitter_user')

## Auto-Follow forms
class FollowTwitterAccountForm(forms.ModelForm):
	class Meta:
		model = Follow
		fields = ('twitter_follow_account_id', )

class AddAttachedTwitterAccountForFollowForm(forms.ModelForm):
	class Meta:
		model = TwitterAttachedAccountsForFollow
		fields = ('added_by_twitter_user', 'twitter_username', )

class AddTwitterAutoFollowMetaForm(forms.ModelForm):
	class Meta:
		model = TwitterAutoFollowMeta
		fields = ('added_by_twitter_user', 'auto_follow_phrase', )

## Auto-retweet forms
class AddAttachedTwitterAccountForRetweetForm(forms.ModelForm):
	class Meta:
		model = TwitterAttachedAccountsForRetweet
		fields = ('added_by_twitter_user', 'twitter_username', )

class RetweetTwitterAccountForm(forms.ModelForm):
	class Meta:
		model = Retweet
		fields = ('twitter_retweet_account_id', 'added_by_twitter_user')


class AddAttachedTwitterAccountForAnalyzerForm(forms.ModelForm):
	class Meta:
		model = TwitterAttachedAccountsForAnalyzer
		fields = ('added_by_twitter_user', 'twitter_username', )