import os
from django.db import models
from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
from django.contrib.postgres.fields import JSONField
from django.utils.crypto import get_random_string

def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'key_{0}.json'.format(get_random_string(32))

class SiteConfig(models.Model):
    CONFIG_TYPE_CHOICES = (
        ('S', 'Site'),
        ('T', 'Tool'),
    )
    key = models.TextField(max_length=100, blank=True, unique=True)
    config = JSONField()
    config_type = models.CharField(max_length=1, choices=CONFIG_TYPE_CHOICES, default='S')
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)

class ToolStats(models.Model):
    key = models.TextField(max_length=100, blank=True)
    end_time = models.DateTimeField(null=True, blank=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)

class TwitterUser(models.Model):
    user = models.ManyToManyField(User)
    is_active = models.BooleanField(default=True)
    twitter_username = models.TextField(max_length=100, blank=True)
    twitter_consumer_key = models.TextField(max_length=100, blank=True)
    twitter_consumer_secret = models.TextField(max_length=100, blank=True)
    twitter_access_token = models.TextField(max_length=100, blank=True)
    twitter_access_token_secret = models.TextField(max_length=100, blank=True)
    twitter_auto_response_message = models.TextField(blank=True)
    config = JSONField(null=True, default=[{'auto_post': True, 'auto_follow': True, 'auto_unfollow': True, 'auto_retweet': True, 'auto_direct_message': True, 'auto_twitter_analyzer': True}])
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)

## Twitter user stats
class TwitterUserStats(models.Model):
    added_by_twitter_user = models.ForeignKey(TwitterUser)
    stats = JSONField()
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)

##Clicnet secret is the secret json fiel per suer, for sharing google sheets data
## Blank tru here in client_secret is a blunder sort of a hack
class TwitterAutoPostMeta(models.Model):
    added_by_twitter_user = models.ForeignKey(TwitterUser, blank
        =True)
    client_secret = models.FileField(upload_to=user_directory_path, blank=True)
    google_sheets_file_name = models.TextField(blank=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)
    def delete(self, *args, **kwargs):
        os.remove(os.path.join(settings.MEDIA_ROOT, self.client_secret.name))
        super(Document,self).delete(*args,**kwargs)

class Tweet(models.Model):
    added_by_twitter_user = models.ForeignKey(TwitterUser)
    message = models.TextField(max_length=140)
    col_processed = models.PositiveSmallIntegerField(default=0)
    status = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)

class TwitterAttachedAccountsForFollow(models.Model):
    added_by_twitter_user = models.ForeignKey(TwitterUser)
    twitter_username = models.TextField(max_length=140)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)

class Follow(models.Model):
    # Source account => The user who initiated the follow request
    # Destination => The twitter account that as requested to be followed
    # To keep track of the responses post following an account
    FOLLOW_RESPONSE_CHOICES = (
        ('U', 'Unfollow'),
        ('F', 'Follow-back'),
    )
    # To keep track of the follow status from the source account
    FRIENDSHIP_CHOICES = (
        ('U', 'Unfollow'),
        ('F', 'Follow'),
    )
    added_by_twitter_user = models.ForeignKey(TwitterUser, blank=True)
    twitter_follow_account_id = models.CharField(max_length=140, blank=True)
    twitter_follow_phrase = models.CharField(max_length=140, blank=True)
    follow_response = models.CharField(max_length=1, choices=FOLLOW_RESPONSE_CHOICES, default='U')
    follow_status = models.CharField(max_length=1, choices=FRIENDSHIP_CHOICES, default='F')
    follow_phrase = models.TextField(blank=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)

##For Auto Follow Phrases
class TwitterAutoFollowMeta(models.Model):
    added_by_twitter_user = models.ForeignKey(TwitterUser)
    auto_follow_phrase = models.CharField(max_length=140, blank=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)

# Auto Retweet Models
class Retweet(models.Model):
    ## To keep track of the retweet status
    added_by_twitter_user = models.ForeignKey(TwitterUser, blank=True)
    twitter_retweet_account_id = models.CharField(max_length=140, blank=True)
    retweet_status = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)


class TwitterAttachedAccountsForRetweet(models.Model):
    added_by_twitter_user = models.ForeignKey(TwitterUser, blank
        =True)
    twitter_username = models.TextField(max_length=140)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)

class TwitterAttachedAccountsForAnalyzer(models.Model):
    added_by_twitter_user = models.ForeignKey(TwitterUser, blank
        =True)
    twitter_username = models.TextField(max_length=140)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)

## Auto DirectMessage Models
class DirectMessage(models.Model):
    ## To keep track of the source twitter user account
    added_by_twitter_user = models.ForeignKey(TwitterUser)
    twitter_direct_message_account_id = models.CharField(max_length=140, blank=True)
    message = models.TextField()
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)

class TwitterAutoDirectMessageMeta(models.Model):
    added_by_twitter_user = models.ForeignKey(TwitterUser)
    twitter_auto_response_message = models.TextField(blank=True)
    auto_delete_direct_messages = models.BooleanField(default=False)
    auto_delete_direct_messages_older_than_days = models.TextField(blank=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)

## auto follow settings
#  follow count per account
#  max follow count
#  is_active

## auto retweet settings
#  retweet_only_first_tweet
#  is active

## auto dm
#  enable_clear_direct_messages
