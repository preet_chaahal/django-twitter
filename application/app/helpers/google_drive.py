from oauth2client.service_account import ServiceAccountCredentials
from httplib2 import Http
from apiclient.discovery import build

def find_image_url(folder_name, secret_file):

    if not folder_name:
        return False
    
    image_folder_name = folder_name
    image_folder_id = None

    scopes = ['https://www.googleapis.com/auth/drive.metadata.readonly']

    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        secret_file, scopes)

    http_auth = credentials.authorize(Http())

    driveadmin = build('drive', 'v3', http=http_auth)

    add_parents = 'My Logo'

    file_metadata = {
      'name' : 'photos',
      'mimeType' : 'application/vnd.google-apps.photos'
    }

    results = driveadmin.files().list(
    	fields="nextPageToken, files(id, name, iconLink, webViewLink)", pageSize=10, q="mimeType='application/vnd.google-apps.folder'").execute()
    items = results.get('files', [])
    if not items:
        print('No files found.')
    else:
        for item in items:
        	if item['name'] == image_folder_name:
        		image_folder_id = item['id']
            	# print('{0} ({1} {2} {3})'.format(item['name'], item['id'], item['iconLink'], item['webViewLink']))

    ##Fetching all files from a given folder
    query_to_filter_images_inside_given_folder = str("'"+ image_folder_id+"' in parents")

      
    print(query_to_filter_images_inside_given_folder)

    images_for_auto_post = driveadmin.files().list(
    	fields="nextPageToken, files(id, name, iconLink, webViewLink, parents)", q=query_to_filter_images_inside_given_folder, pageSize=10).execute()
    images = images_for_auto_post.get('files', [])

    if not images:
        print('No images found.')
    else:
        print('Images:')
        for image in images:
    		print('{0} ({1} {2} {3} {4})'.format(image['name'], image['id'], image['iconLink'], image['webViewLink'], image['parents']))