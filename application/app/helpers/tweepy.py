from django.http import HttpResponse
import os
from requests import get
import re
from urllib import request
from django.contrib.auth.decorators import login_required
from site_dashboard.models import TwitterAttachedAccountsForFollow
from site_dashboard.models import TwitterAttachedAccountsForRetweet
from site_dashboard.models import TwitterUser,TwitterAutoFollowMeta
from site_dashboard.models import Follow
from site_dashboard.models import TwitterAutoPostMeta
from .generic import is_tool_active, is_tool_activated_by_user, fetch_tool_config, get_filename_from_cd
from django.conf import settings
from site_dashboard.models import Tweet
import requests
import tweepy
import json
import numpy
import collections
import datetime
import gspread
from django.core.files.storage import FileSystemStorage
from oauth2client.service_account import ServiceAccountCredentials


#Global kind of a constructor
def init_tweepy(consumer_key, consumer_secret, access_token, access_token_secret):
	auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
	auth.set_access_token(access_token, access_token_secret)
	api = tweepy.API(auth, wait_on_rate_limit_notify=True, wait_on_rate_limit=True)
	return api

def post_tweet(twitter_user, tweet_data):
	context = {
				'status': 0,
				'message': 'Invalid request!'
			}
	#Fetch access token and other info to post a tweet
	consumer_key = twitter_user.twitter_consumer_key
	consumer_secret = twitter_user.twitter_consumer_secret
	access_token = twitter_user.twitter_access_token
	access_token_secret = twitter_user.twitter_access_token_secret
	try:
		auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
		auth.set_access_token(access_token, access_token_secret)
		api = tweepy.API(auth, wait_on_rate_limit=True)
		api.update_status(tweet_data['message'])
		return True
	except tweepy.TweepError as e:
		data = json.loads(e.reason.replace("[", "").replace("]", "").replace("'", "\""))
		error_code = data['code']
		error_message = data['message']
		return error_message

##Auto-post tweet from google sheets
def auto_post_tweet():
	if not is_tool_active('auto_post'):
		return {'status': False, 'message': 'The tool is inactive for now!'}
	twitter_users = TwitterAutoPostMeta.objects.filter(is_active=True).select_related()

	conf = fetch_tool_config('auto_post')
	status = False
	post_count = 0
	errors = []

	for twitter_user_meta in twitter_users:
		##Check if current user has activated the tool
		if not is_tool_activated_by_user('auto_post', twitter_user_meta.id):
			continue
		
		try:
			api = init_tweepy(twitter_user_meta.added_by_twitter_user.twitter_consumer_key, twitter_user_meta.added_by_twitter_user.twitter_consumer_secret, twitter_user_meta.added_by_twitter_user.twitter_access_token, twitter_user_meta.added_by_twitter_user.twitter_access_token_secret)
			me = api.me()
		except Exception as e:
			errors.append(e)
			status = False
			errors.append(e)
			continue
		
		## Get file from twitter user
		try:
			secret_file_name = twitter_user_meta.client_secret.name 
			file_name = settings.MEDIA_ROOT + '/' + secret_file_name
			sheets_file_name = twitter_user_meta.google_sheets_file_name
			scope = ['https://spreadsheets.google.com/feeds']
		except Exception as e:
			errors.append(e)
			status = False
			errors.append(e)
			print(e)
			continue

		## Fetching position of last tweeted column
		##Assume to start from the first position
		curr_position = 1
		try:
			q = Tweet.objects.filter(added_by_twitter_user_id=twitter_user_meta.added_by_twitter_user.id).order_by('-created_at')[:1]
			curr_position = int(q[0].col_processed)
		except Exception as e:
			errors.append(e)
			print(e)
			flag = False
		
		try:
			credentials = ServiceAccountCredentials.from_json_keyfile_name(file_name, scope)
			gc = gspread.authorize(credentials)
			wks = gc.open(sheets_file_name).sheet1
		except Exception as e:
			errors.append(e)
			status = False
			errors.append(e)
			print(e)
			continue
		
		total_cols = wks.col_values(1)
		total_cols = len(total_cols)
		
		if curr_position+1 > total_cols:
			curr_position = 1

		target_cell = wks.cell(curr_position, 1)
		target_cell_for_image = wks.cell(curr_position, 2).input_value
		tweet_message = target_cell.input_value

		##Fetching image for posting

		## Auto post tweet works by first trying to post tweet along with url, which is a public share-able link from gogole drive
		## Incase the media rendering has some exception, the tweet is posted as text, ignoring the media

		if tweet_message:
			
			image_url = target_cell_for_image

			flag = False
			if image_url.startswith('https://drive.google.com/open?id='):
				image_found = False
				try:
					# image_url.replace('/open?id=', '/uc?id=')
					# print("Originla url: "+ image_url)
					image_url = image_url.replace('/open?id=', '/uc?export=download&id=')
					
					# print("Url="+ str(image_url))
					r = requests.get(image_url)
					filename = get_filename_from_cd(r)
					# print("Filename returned=:")
					# print(str(filename))

					temp_downloaded_image_uri = settings.MEDIA_ROOT +"/"+ filename
					open(temp_downloaded_image_uri, 'wb').write(r.content)
					
					image_found = True
				except Exception as e:
					errors.append(e)
					print(e)
					image_found = False

				status_for_sheet_cell = ''
				if image_found:
					try:
						flag = api.update_with_media(filename=temp_downloaded_image_uri, status=tweet_message)
						flag = True
						status_for_sheet_cell =  'Tweet posted with media.'
					except Exception as e:
						print(e)
						flag = False
						status_for_sheet_cell = 'Error posting tweet with media.'
				try:
					os.remove(temp_downloaded_image_uri)
				except Exception:
					x = 1

			if not flag:
				try:
					api.update_status(tweet_message)
					status_for_sheet_cell = "Tweet posted without media."
				except Exception as e:
					print(e)
					status_for_sheet_cell += "Error posting tweet."
				
			try:
				wks.update_cell(curr_position, 3, status_for_sheet_cell)
			except Exception:
				x = 1

			## Print in the corresponding exel sheet column about the status
			post_count += 1

		##update the status of the twitterlimit in the db of the current user
		t_obj = Tweet.objects.create(
			message = tweet_message,
			col_processed = int(curr_position + 1),
			added_by_twitter_user_id = twitter_user_meta.added_by_twitter_user.id
		)
		status = True
		

	return {'status': status, 'post_count': post_count, 'errors': errors}

## Auto-direct message
def direct_message_followers():
	
	if not is_tool_active('auto_direct_message'):
		return {'status': False, 'message': 'The tool is inactive for now!'}
	twitter_users = TwitterUser.objects.filter(is_active=True)
	conf = fetch_tool_config('auto_direct_message')
	enable_clearing_direct_messages = conf.config['enable_clearing_direct_messages']

	message_sent_count = 0
	tweepy_errors = []
	returnData = {'status': False, 'message_sent_count': message_sent_count}
	for twitter_user in twitter_users:
		if not is_tool_activated_by_user('auto_direct_message', twitter_user_meta.id):
			continue
		try:
			outer_loop_counter=0		
			api = init_tweepy(twitter_user.twitter_consumer_key, twitter_user.twitter_consumer_secret, twitter_user.twitter_access_token, twitter_user.twitter_access_token_secret)
			me = api.me()
			message = twitter_user.twitter_auto_response_message
		    
		    # Process the friend here
			# my_followers = []
			# for page in tweepy.Cursor(api.followers_ids, id=me.id).pages():
			# 	my_followers.append(page)

			my_friends = []
			for page in tweepy.Cursor(api.friends_ids, id=me.id).pages():
				for friend in page:
					my_friends.append(friend)

			new_followers = []
			for my_follower_page in tweepy.Cursor(api.followers_ids, me.id).pages():
				for my_follower in my_follower_page:
					if my_follower not in my_friends:
						new_followers.append(my_follower)

			## allowed_response = True
			try:
				for new_follower in new_followers:
					api.send_direct_message(new_follower, text=message)
					api.create_friendship(id=new_follower)
					message_sent_count += 1
			except tweepy.TweepError as e:
				tweepy_errors.append(e)

			status = True
			message_sent_count = message_sent_count

		except tweepy.TweepError as e:
			error_code = e.response
			error_message = e.response.text
			tweepy_errors.append(e)
			status = False
			error_code = error_code
			error_message = error_message

	return {'status': status, 'message_sent_count': message_sent_count, 'tweepy_errors': tweepy_errors}

## Auto-follow helpers
def fetch_attached_followers(twitterUserId = None):	
	if twitterUserId == None:
		##Fetch followers by all users
		followers = TwitterAttachedAccountsForFollow.objects.all()
	else:
		followers = TwitterAttachedAccountsForFollow.objects.filter(added_by_twitter_user=twitterUserId)
	return followers

def follow_accounts():
	if not is_tool_active('auto_follow'):
		return {'status': False, 'message': 'The tool is inactive for now!'}	
	## Fetching auto follow configurations
	conf = fetch_tool_config('auto_follow')
	follow_count_per_account = conf.config['follow_limit_per_run']
	loop_count = 0
	follow_phrase = ''
	##Fethcing all twitter account added to the system
	twitter_users = TwitterUser.objects.filter(is_active=True)
	tweepy_errors = []
	follow_count = 0
	inner_loop_count = 0
	status = False
	## Step 1 Loop through each twitter account associated with this app
	for twitter_user in twitter_users:
		if not is_tool_activated_by_user('auto_follow', twitter_user_meta.id):
			continue
		try:
			api = init_tweepy(twitter_user.twitter_consumer_key, twitter_user.twitter_consumer_secret, twitter_user.twitter_access_token, twitter_user.twitter_access_token_secret)
			me = api.me()
			inner_loop_count = 0
			follow_count = 0
			outer_loop_counter=0
			##Getting list of followers
			my_friends_ids = []
			for friends_page in tweepy.Cursor(api.friends_ids).pages():
				for friend in friends_page:
					my_friends_ids.append(friend)

			my_friends_ids.append(me.id)

			current_users_twitter_ids_to_follow = TwitterAttachedAccountsForFollow.objects.filter(added_by_twitter_user_id=twitter_user.id)
			
			for twitter_id_to_follow in current_users_twitter_ids_to_follow:
				inner_loop_count = 0
				for current_user_followers_page in tweepy.Cursor(api.followers_ids, twitter_id_to_follow.twitter_username).pages():
					for current_user_follower in current_user_followers_page:
						if (inner_loop_count == follow_count_per_account):
							break
						if current_user_follower not in my_friends_ids:
							flag = False
							try:
								api.create_friendship(id=current_user_follower)
								flag = True
							except tweepy.TweepError as e:
								tweepy_errors.append(e)

							if flag:
								follow_count += 1
								inner_loop_count += 1					

			status = True
			loop_count = loop_count
			inner_loop_count = inner_loop_count
			tweepy_errors = tweepy_errors
			# returnData = {'status': True, 'follow_count': follow_count}

		except tweepy.TweepError as e:
			# error_code = e.response
			# error_message = e.response.text
			tweepy_errors.append(e)
			status = False
			# error_message = error_message
			follow_count = follow_count
			# returnData = False

	return {'status': status, 'follow_count': follow_count, 'tweepy_errors': tweepy_errors, 'inner_loop_count': inner_loop_count}

def follow_phrase():
	if not is_tool_active('auto_follow_phrase'):
		return {'status': False, 'message': 'The tool is inactive for now!'}	
	## Fetching auto follow configurations
	conf = fetch_tool_config('auto_follow')
	follow_count_per_account = conf.config['follow_limit_per_run']
	loop_count = 0
	follow_phrase = ''
	##Fethcing all twitter account added to the system
	twitter_users = TwitterUser.objects.filter(is_active=True)
	tweepy_errors = []
	follow_count = 0
	inner_loop_count = 0
	status = False
	## Step 1 Loop through each twitter account associated with this app
	for twitter_user in twitter_users:
		if not is_tool_activated_by_user('auto_follow_phrase', twitter_user_meta.id):
			continue
		try:
			api = init_tweepy(twitter_user.twitter_consumer_key, twitter_user.twitter_consumer_secret, twitter_user.twitter_access_token, twitter_user.twitter_access_token_secret)
			me = api.me()
			inner_loop_count = 0
			follow_count = 0
			outer_loop_counter=0
			##Getting list of followers
			my_friends_ids = []
			for friends_page in tweepy.Cursor(api.friends_ids).pages():
				for friend in friends_page:
					my_friends_ids.append(friend)

			my_friends_ids.append(me.id)

			current_users_twitter_phrases_for_follow = TwitterAutoFollowMeta.objects.filter(added_by_twitter_user_id=twitter_user.id)
			
			for twitter_phrase_for_follow in current_users_twitter_phrases_for_follow:
				inner_loop_count = 0
				current_follow_count = 0

				for follower in tweepy.Cursor(api.search, q=twitter_phrase_for_follow.auto_follow_phrase).items():
					if current_follow_count >= follow_count_per_account:
						break
					try:
						api.create_friendship(screen_name = follower.screen_name)
						current_follow_count += 1
					except Exception as e:
						tweepy_errors.append(e)
						continue
					outer_loop_counter += current_follow_count

			status = True
			# returnData = {'status': True, 'follow_count': follow_count}

		except tweepy.TweepError as e:
			# error_code = e.response
			# error_message = e.response.text
			tweepy_errors.append(e)
			status = False
			# error_message = error_message
			follow_count = follow_count
			# returnData = False

	return {'status': status, 'follow_count': follow_count, 'tweepy_errors': tweepy_errors, 'total_follows': outer_loop_counter}

def unfollow_accounts():
	if not is_tool_active('auto_unfollow'):
		return {'status': False, 'message': 'The tool is inactive for now!'}	
	## Fetching auto follow configurations
	conf = fetch_tool_config('auto_unfollow')
	unfollow_count_per_account = conf.config['unfollow_limit_per_run']
	loop_count = 0
	##Fethcing all twitter account added to the system
	twitter_users = TwitterUser.objects.filter(is_active=True)
	tweepy_errors = []
	unfollow_count = 0
	inner_loop_count = 0
	status = False
	## Step 1 Loop through each twitter account associated with this app
	for twitter_user in twitter_users:
		if not is_tool_activated_by_user('auto_unfollow', twitter_user_meta.id):
			continue
		try:
			api = init_tweepy(twitter_user.twitter_consumer_key, twitter_user.twitter_consumer_secret, twitter_user.twitter_access_token, twitter_user.twitter_access_token_secret)
			me = api.me()
			inner_loop_count = 0
			follow_count = 0
			outer_loop_counter=0

			current_users_twitter_ids_for_unfollow = Follow.objects.filter(added_by_twitter_user_id=twitter_user.id)
			
			for current_users_twitter_id_for_unfollow in current_users_twitter_ids_for_unfollow:
				current_unfollow_count = 0
				if current_unfollow_count >= unfollow_count_per_account:
					break
				try:
					api.destroy_friendship(current_users_twitter_id_for_unfollow.twitter_follow_account_id)
					usr = Follow.objects.get(pk=current_users_twitter_id_for_unfollow.id)
					usr.follow_status = 'U'
					usr.save()
				except Exception as e:
					tweepy_errors.append(e)
					continue

				unfollow_count += current_unfollow_count

			status = True
			
		except tweepy.TweepError as e:
			# error_code = e.response
			# error_message = e.response.text
			tweepy_errors.append(e)
			status = False

			
	return {'status': status, 'unfollow_count': unfollow_count, 'tweepy_errors': tweepy_errors}	
	
def save_follow(added_by_twitter_id, twitter_account_id, follow_phrase):
	added_by_twitter_user = TwitterUser.objects.get(pk=added_by_twitter_id)
	follow = Follow(added_by_twitter_user=added_by_twitter_user, twitter_follow_account_id=twitter_account_id, twitter_follow_phrase=follow_phrase)
	return follow.save()

def unfollow_accounts(twitter_user_id, twitter_ids_to_unfollow=False, unfollow_count_per_account=10):
	#Fetch access token and other info to post a tweet
	unfollow_count_per_account = int(unfollow_count_per_account)
	twitter_user = TwitterUser.objects.get(pk=twitter_user_id)
	consumer_key = twitter_user.twitter_consumer_key
	consumer_secret = twitter_user.twitter_consumer_secret
	access_token = twitter_user.twitter_access_token
	access_token_secret = twitter_user.twitter_access_token_secret
	unfollow_count = 0
	returnData = {'status': True, 'unfollow_count': unfollow_count}
	try:
		auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
		auth.set_access_token(access_token, access_token_secret)
		api = tweepy.API(auth, wait_on_rate_limit=True)
		me = api.me()
		outer_loop_counter=0		
		for friend in tweepy.Cursor(api.friends_ids).items():
			api.destroy_friendship(friend)
			unfollow_count += 1
			
		returnData = {'status': True, 'unfollow_count': unfollow_count}

	except tweepy.TweepError as e:
		error_code = e.response
		error_message = e.response.text
		returnData = {'status': False, 'error_code': error_code, 'error_message': error_message}

	return returnData

def retweet_accounts():
	if not is_tool_active('auto_retweet'):
		return {'status': False, 'message': 'The tool is inactive for now!'}
	conf = fetch_tool_config('auto_retweet')
	retweet_count = 0
	retweet_done = False		
	status = False
	error_message = False
	error_code = False
	allow_duplicate_retweet = conf.config['skip_duplicate_retweet']
	
	##Fethcing all twitter account added to the system
	twitter_users = TwitterUser.objects.filter(is_active=True)
	returnData = False
	for twitter_user in twitter_users:
		if not is_tool_activated_by_user('auto_retweet', twitter_user_meta.id):
			continue
		try:
			api = init_tweepy(twitter_user.twitter_consumer_key, twitter_user.twitter_consumer_secret, twitter_user.twitter_access_token, twitter_user.twitter_access_token_secret)
			me = api.me()
			follow_count_per_account = 0
			##Getting list of followers
			current_user_list_for_retweet_account = TwitterAttachedAccountsForRetweet.objects.filter(added_by_twitter_user_id=twitter_user.id, is_active=True)
			
			for account_for_retweet in current_user_list_for_retweet_account:
				inner_loop_count = 0			
				twitter_account_status_objects = api.user_timeline(id=account_for_retweet.twitter_username, count=200)
				loopcounter = 0
				retweet_done = False
				for twitter_account_status_object in twitter_account_status_objects:
					if retweet_done:
						break
					if allow_duplicate_retweet or not twitter_account_status_object.retweeted:
						api.retweet(twitter_account_status_object.id)
						retweet_done = True
						## Perform Retweet
						status = True
						retweet_count += 1#twitter_account_status_object.retweet_count 
					loopcounter += 1

			# returnData = {'status': True, 'follow_count': follow_count}

		except tweepy.TweepError as e:
			error_code = e.response
			# error_message = e.response.text
			# returnData = {'status': False, 'error_code': error_code, 'error_message': error_message, 'follow_count': 0}
			returnData = False

	return {'status': status, 'retweet_count': retweet_count, 'error_code': error_code, 'error_message': error_message}
	

def fetch_attached_retweet_accounts(twitterUserId = None):	
	if twitterUserId == None:
		##Fetch retweet_accounts by all users
		retweet_accounts = TwitterAttachedAccountsForRetweet.objects.all()
	else:
		retweet_accounts = TwitterAttachedAccountsForRetweet.objects.filter(added_by_twitter_user=twitterUserId)
	return retweet_accounts
