from site_dashboard.models import SiteConfig
from site_dashboard.models import TwitterUser
from os.path import basename
from django.conf import settings
import re
import cgi

def is_tool_active(tool_key):
    obj = SiteConfig.objects.get(key = tool_key)
    try:
        if obj.config['tool_status']:
            return True
        else:
            return False
    except Exception:
        return False

##Params is TwitterUserObject
def is_tool_activated_by_user(tool_key, user_id):
    obj = TwitterUser.objects.get(key = user_id)
    try:
        if obj.config[tool_key]:
            return True
        else:
            return False
    except Exception:
        return False

def fetch_tool_config(tool_key):
	return SiteConfig.objects.get(key = tool_key)


def get_filename_from_cd(cd):
    """
    Get filename from content-disposition
    """
    if not cd:
        return None

    try:
        return cgi.parse_header(cd.headers['content-disposition'])[-1]['filename']
    except Exception as e:
        print(e)
        return False