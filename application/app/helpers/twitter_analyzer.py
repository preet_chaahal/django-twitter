#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2017 @x0rz
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# Usage:
# python tweets_analyzer.py -n screen_name
#
# Install:
# pip install tweepy ascii_graph tqdm numpy

from __future__ import unicode_literals

from site_dashboard.models import TwitterUser
from site_dashboard.models import TwitterUserStats
from .generic import is_tool_active
from .generic import fetch_tool_config
from operator import itemgetter, attrgetter
import tweepy
import numpy
import collections
import datetime
import json

try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse

# Here are globals used to store data - I know it's dirty, whatever
start_date = 0
end_date = 0

activity_hourly = {
    ("%2i:00" % i).replace(" ", "0"): 0 for i in range(24)
}

activity_weekly = {
    "%i" % i: 0 for i in range(7)
}

detected_langs = collections.Counter()
detected_sources = collections.Counter()
detected_places = collections.Counter()
geo_enabled_tweets = 0
detected_hashtags = collections.Counter()
detected_domains = collections.Counter()
detected_timezones = collections.Counter()
retweets = 0
retweeted_users = collections.Counter()
mentioned_users = collections.Counter()
id_screen_names = {}
friends_timezone = collections.Counter()
friends_lang = collections.Counter()

def set_default(obj):
    if isinstance(obj, set):
        return list(obj)
    raise TypeError

#Global kind of a constructor
def init_tweepy(consumer_key, consumer_secret, access_token, access_token_secret):
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth, wait_on_rate_limit_notify=True, wait_on_rate_limit=True)
    return api


def process_tweet(tweet):
    """ Processing a single Tweet and updating our datasets """
    global start_date
    global end_date
    global geo_enabled_tweets
    global retweets

    # Check for filters before processing any further
    # if args.filter and tweet.source:
    #     if not args.filter.lower() in tweet.source.lower():
    #         return

    tw_date = tweet.created_at

    # Updating most recent tweet
    end_date = end_date or tw_date
    start_date = tw_date

    # Handling retweets
    try:
        # We use id to get unique accounts (screen_name can be changed)
        rt_id_user = tweet.retweeted_status.user.id_str
        retweeted_users[rt_id_user] += 1

        if tweet.retweeted_status.user.screen_name not in id_screen_names:
            id_screen_names[rt_id_user] = "@%s" % tweet.retweeted_status.user.screen_name

        retweets += 1
    except:
        pass

    # Adding timezone from profile offset to set to local hours
    # if tweet.user.utc_offset and not args.no_timezone:
    if tweet.user.utc_offset:
        tw_date = (tweet.created_at + datetime.timedelta(seconds=tweet.user.utc_offset))

    # if args.utc_offset:
    #     tw_date = (tweet.created_at + datetime.timedelta(seconds=args.utc_offset))

    # Updating our activity datasets (distribution maps)
    activity_hourly["%s:00" % str(tw_date.hour).zfill(2)] += 1
    activity_weekly[str(tw_date.weekday())] += 1

    # Updating langs
    detected_langs[tweet.lang] += 1

    # Updating sources
    detected_sources[tweet.source] += 1

    # Detecting geolocation
    if tweet.place:
        geo_enabled_tweets += 1
        tweet.place.name = tweet.place.name
        detected_places[tweet.place.name] += 1

    # Updating hashtags list
    if tweet.entities['hashtags']:
        for ht in tweet.entities['hashtags']:
            ht['text'] = "#%s" % ht['text']
            detected_hashtags[ht['text']] += 1

    # Updating domains list
    if tweet.entities['urls']:
        for url in tweet.entities['urls']:
            domain = urlparse(url['expanded_url']).netloc
            if domain != "twitter.com":  # removing twitter.com from domains (not very relevant)
                detected_domains[domain] += 1

    # Updating mentioned users list
    if tweet.entities['user_mentions']:
        for ht in tweet.entities['user_mentions']:
            mentioned_users[ht['id_str']] += 1
            if not ht['screen_name'] in id_screen_names:
                id_screen_names[ht['id_str']] = "@%s" % ht['screen_name']

    

def process_friend(friend):
    """ Process a single friend """
    friends_lang[friend.lang] += 1 # Getting friend language & timezone
    if friend.time_zone:
        friends_timezone[friend.time_zone] += 1


def get_friends(api, username, limit):
    """ Download friends and process them """
    for friend in tweepy.Cursor(api.friends, screen_name=username).items(limit):
        process_friend(friend)


def get_tweets(api, username, limit):
    """ Download Tweets from username account """
    for status in tweepy.Cursor(api.user_timeline, screen_name=username).items(limit):
        process_tweet(status)


def int_to_weekday(day):
    weekdays = "Monday Tuesday Wednesday Thursday Friday Saturday Sunday".split()
    return weekdays[int(day) % len(weekdays)]

def print_stats(dataset, top=5):
    """ Displays top values by order """
    sum = numpy.sum(list(dataset.values()))
    i = 0
    if sum:
        sorted_keys = sorted(dataset, key=dataset.get, reverse=True)
        max_len_key = max([len(x) for x in sorted_keys][:top])  # use to adjust column width
        for k in sorted_keys:
            try:
                print(("- \033[1m{:<%d}\033[0m {:>6} {:<4}" % max_len_key)
                      .format(k, dataset[k], "(%d%%)" % ((float(dataset[k]) / sum) * 100)))
            except:
                import ipdb
                ipdb.set_trace()
            i += 1
            if i >= top:
                break
    else:
        print("No data")
    print("")


def print_charts(dataset, title, weekday=False):
    """ Prints nice charts based on a dict {(key, value), ...} """
    chart = []
    keys = sorted(dataset.keys())
    mean = numpy.mean(list(dataset.values()))
    median = numpy.median(list(dataset.values()))

    for key in keys:
        if (dataset[key] >= median * 1.33):
            displayed_key = "%s (\033[92m+\033[0m)" % (int_to_weekday(key) if weekday else key)
        elif (dataset[key] <= median * 0.66):
            displayed_key = "%s (\033[91m-\033[0m)" % (int_to_weekday(key) if weekday else key)
        else:
            displayed_key = (int_to_weekday(key) if weekday else key)

        chart.append((displayed_key, dataset[key]))

    thresholds = {
        int(mean): Gre, int(mean * 2): Yel, int(mean * 3): Red,
    }
    data = hcolor(chart, thresholds)

    graph = Pyasciigraph(
        separator_length=4,
        multivalue=False,
        human_readable='si',
    )

    for line in graph.graph(title, data):
        print(line)
    print("")

def main_app():

    if not is_tool_active('auto_twitter_analyzer'):
        return {'status': False, 'message': 'The tool is inactive for now!'}
    twitter_users = TwitterUser.objects.filter(is_active=True)
    conf = fetch_tool_config('auto_twitter_analyzer')
    max_limit_for_tweet_analysis = int(conf.config['max_limit_for_tweet_analysis'])
    
    ##Fethcing all twitter account added to the system
    twitter_users = TwitterUser.objects.filter(is_active=True)

    tweepy_errors = []
    follow_count = 0
    inner_loop_count = 0
    status = False

    for twitter_user in twitter_users:
        try:
            # Here are globals used to store data - I know it's dirty, whatever
            # start_date = 0
            # end_date = 0

            # activity_hourly = {
            #     ("%2i:00" % i).replace(" ", "0"): 0 for i in range(24)
            # }

            # activity_weekly = {
            #     "%i" % i: 0 for i in range(7)
            # }

            # detected_langs = collections.Counter()
            # detected_sources = collections.Counter()
            # detected_places = collections.Counter()
            # geo_enabled_tweets = 0
            # detected_hashtags = collections.Counter()
            # detected_domains = collections.Counter()
            # detected_timezones = collections.Counter()
            # retweets = 0
            # retweeted_users = collections.Counter()
            # mentioned_users = collections.Counter()
            # id_screen_names = {}
            # friends_timezone = collections.Counter()
            # friends_lang = collections.Counter()

            twitter_api = init_tweepy(twitter_user.twitter_consumer_key, twitter_user.twitter_consumer_secret, twitter_user.twitter_access_token, twitter_user.twitter_access_token_secret)
            me = twitter_api.me()
            inner_loop_count = 0
            follow_count = 0
            outer_loop_counter=0
            tweets_limit_for_analysis = 10
            average_tweets_per_day = False

            # Getting general account's metadata
            user_info = twitter_api.get_user(id=twitter_user.twitter_username)

            lang = user_info.lang
            geo_enabled = user_info.geo_enabled
            time_zone = user_info.time_zone
            utc_offset = user_info.utc_offset

            status_count = user_info.statuses_count

            # Will retreive all Tweets from account (or max limit)
            if max_limit_for_tweet_analysis > 0 and max_limit_for_tweet_analysis <= user_info.statuses_count:
                num_tweets = max_limit_for_tweet_analysis
            else:
                numpy.amin([tweets_limit_for_analysis, user_info.statuses_count])
                
            # Download tweets
            get_tweets(twitter_api, twitter_user.twitter_username, limit=num_tweets)
            # print("[+] Downloaded %d tweets from %s to %s (%d days)" % (num_tweets, start_date, end_date, (end_date - start_date).days))
            tweet_stats_from = start_date
            tweet_stats_to = end_date
            tweet_stats_for = (end_date - start_date).days

            # Checking if we have enough data (considering it's good to have at least 30 days of data)
            # if (end_date - start_date).days < 30 and (num_tweets < user_info.statuses_count):
            #      print("[\033[91m!\033[0m] Looks like we do not have enough tweets from user, you should consider retrying (--limit)")

            if (end_date - start_date).days != 0:
                average_tweets_per_day = num_tweets / float((end_date - start_date).days)
                # print("[+] Average number of tweets per day: \033[1m%.1f\033[0m" % (num_tweets / float((end_date - start_date).days)))

            # Print activity distrubution charts
            # print_charts(activity_hourly, "Daily activity distribution (per hour)")
            # print_charts(activity_weekly, "Weekly activity distribution (per day)", weekday=True)

            # print("[+] Detected languages (top 5)")
            # print_stats(detected_langs)

            # print("[+] Detected sources (top 10)")
            # print_stats(detected_sources, top=10)

            # print("[+] There are \033[1m%d\033[0m geo enabled tweet(s)" % geo_enabled_tweets)
            # if len(detected_places) != 0:
                # print("[+] Detected places (top 10)")
                # print_stats(detected_places, top=10)

            # print("[+] Top 10 hashtags")
            # print_stats(detected_hashtags, top=10)

            # print("[+] @%s did \033[1m%d\033[0m RTs out of %d tweets (%.1f%%)" % (twitter_user.twitter_username, retweets, num_tweets, (float(retweets) * 100 / num_tweets)))

            # Converting users id to screen_names
            retweeted_users_names = {}
            for k in retweeted_users.keys():
                retweeted_users_names[id_screen_names[k]] = retweeted_users[k]

            # print("[+] Top 5 most retweeted users")
            # print_stats(retweeted_users_names, top=5)

            mentioned_users_names = {}
            for k in mentioned_users.keys():
                mentioned_users_names[id_screen_names[k]] = mentioned_users[k]
            # print("[+] Top 5 most mentioned users")
            # print_stats(mentioned_users_names, top=5)

            # print("[+] Most referenced domains (from URLs)")
            # print_stats(detected_domains, top=6)

            # if args.friends:
            max_friends = numpy.amin([user_info.friends_count, 300])
            # print("[+] Getting %d @%s's friends data..." % (max_friends, twitter_user.twitter_username))
            # try:

            get_friends(twitter_api, twitter_user.twitter_username, limit=max_friends)
            # except tweepy.error.TweepError as e:
                # if e[0][0]['code'] == 88:
                    # print("[\033[91m!\033[0m] Rate limit exceeded to get friends data, you should retry in 15 minutes")
                # raise

            # print("[+] Friends languages")
            # print_stats(friends_lang, top=6)

            # print("[+] Friends timezones")
            # print_stats(friends_timezone, top=8)

            tweet_stats={
                'lang': lang,
                'geo_enabled': geo_enabled,
                'time_zone': time_zone,
                'utc_offset': utc_offset,
                'status_count': status_count,
                
                'detected_langs': sorted(detected_langs),
                'detected_sources': sorted(detected_sources),
                
                'geo_enabled_tweets': geo_enabled_tweets,
                
                'detected_hashtags': sorted(detected_hashtags),
                
                'retweeted_users_names': sorted(retweeted_users_names),
                'mentioned_users_names': sorted(mentioned_users_names),
                'detected_domains': detected_domains,
                
                'retweets': retweets,
                
                'tweet_stats_from': str(tweet_stats_from),
                'tweet_stats_to':str(tweet_stats_to),
                'tweet_stats_for_days': tweet_stats_for,
                
                'activity_hourly': activity_hourly,
                'activity_weekly': activity_weekly,
                'average_tweets_per_day': average_tweets_per_day
            }

            TwitterUserStats.objects.create(
                added_by_twitter_user = TwitterUser.objects.get(pk=twitter_user.id),
                stats = tweet_stats,
            )

        except tweepy.TweepError as e:
            error_code = e

    return {'message': 'The task was successfully completed!'}