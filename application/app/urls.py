from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import include, url
from django.contrib import admin
from . import views

urlpatterns = [
	url(r'^', include('site_dashboard.urls')),
	url(r'^', include('site_users.urls')),

    url(r'^roles/view', views.roles_view, name='roles_view'),
    # url(r'^site-roles', views.site_roles, name='site_roles'),

    url(r'^admin/', admin.site.urls),
    url(r'^cronjobs/reset/', views.reset_all_automatic_tasks, name='reset_all_automatic_tasks'),
    url(r'^config/tools/reset/', views.config_tools_reset, name='config_tools_reset'),
    
    url(r'^config/twitter-tools-settings', views.twitter_tools_settings, name='twitter_tools_settings'),
    url(r'^config/tools/auto-post', views.config_tools_auto_post, name='config_tools_auto_post'),
    url(r'^config/auto-post-update-config', views.config_tools_auto_post_update_config, name='config_tools_auto_post_update_config'),
    url(r'^config/auto-post-update-schedule', views.config_tools_auto_post_update_schedule, name='config_tools_auto_post_update_schedule'),

    url(r'^config/tools/auto-follow', views.config_tools_auto_follow, name='config_tools_auto_follow'),
    url(r'^config/auto-follow-update-config', views.config_tools_auto_follow_update_config, name='config_tools_auto_follow_update_config'),
    url(r'^config/auto-follow-update-schedule', views.config_tools_auto_follow_update_schedule, name='config_tools_auto_follow_update_schedule'),

    url(r'^config/auto-follow-phrase-update-config', views.config_tools_auto_follow_phrase_update_config, name='config_tools_auto_follow_phrase_update_config'),
    url(r'^config/auto-follow-phrase-update-schedule', views.config_tools_auto_follow_phrase_update_schedule, name='config_tools_auto_follow_phrase_update_schedule'),
    url(r'^config/auto-unfollow-update-config', views.config_tools_auto_unfollow_update_config, name='config_tools_auto_unfollow_update_config'),
    url(r'^config/auto-unfollow-update-schedule', views.config_tools_auto_unfollow_update_schedule, name='config_tools_auto_unfollow_update_schedule'),

    url(r'^config/tools/auto-retweet', views.config_tools_auto_retweet, name='config_tools_auto_retweet'),
    url(r'^config/auto-retweet-update-config', views.config_tools_auto_retweet_update_config, name='config_tools_auto_retweet_update_config'),
    url(r'^config/auto-retweet-update-schedule', views.config_tools_auto_retweet_update_schedule, name='config_tools_auto_retweet_update_schedule'),

    url(r'^config/tools/auto-direct-message', views.config_tools_auto_direct_message, name='config_tools_auto_direct_message'),
    url(r'^config/auto-direct-message-update-config', views.config_tools_auto_direct_message_update_config, name='config_tools_auto_direct_message_update_config'),
    url(r'^config/auto-direct-message-update-schedule', views.config_tools_auto_direct_message_update_schedule, name='config_tools_auto_direct_message_update_schedule'),

    url(r'^config/tools/auto-twitter-analyzer', views.config_tools_auto_twitter_analyzer, name='config_tools_auto_twitter_analyzer'),
    url(r'^config/auto-twitter-analyzer-update-config', views.config_tools_auto_twitter_analyzer_update_config, name='config_tools_auto_twitter_analyzer_update_config'),
    url(r'^config/auto-twitter-analyzer-update-schedule', views.config_tools_auto_twitter_analyzer_update_schedule, name='config_tools_auto_twitter_analyzer_update_schedule'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)