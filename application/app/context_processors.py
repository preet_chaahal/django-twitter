from site_dashboard.models import TwitterUser

def global_vars(request):
	if request.user.is_authenticated:
		twitter_user_accounts = TwitterUser.objects.filter(user=request.user).all()
	else:
		twitter_user_accounts = False
	return {
		'g_twitter_accounts': twitter_user_accounts
	}