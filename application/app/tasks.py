from __future__ import absolute_import, unicode_literals
from datetime import datetime
from celery import Celery
from celery.decorators import periodic_task
from celery.task.schedules import crontab
from django_celery_beat.models import PeriodicTask, IntervalSchedule
from ._celery import app
from app.helpers.tweepy import follow_accounts, follow_phrase, unfollow_accounts, retweet_accounts, direct_message_followers, auto_post_tweet
from app.helpers.twitter_analyzer import  main_app
from celery import shared_task
from site_dashboard.models import ToolStats

def record_start(key):
	r = ToolStats(key=key)
	r.save()
	return r.id

def record_end(id):
	r = ToolStats.objects.get(pk=id)
	r.end_time = datetime.now()
	data = r.save()
	return data

@shared_task
def auto_post_accounts():
	id = record_start('auto_post')
	data = auto_post_tweet()
	record_end(id)
	return data

@shared_task
def auto_follow_accounts():
	## Script to automatically and periodically follow accounts
	id = record_start('auto_follow')
	data = follow_accounts()
	record_end(id)
	return data

@shared_task
def auto_follow_phrase():
	## Script to automatically and periodically follow accounts
	id = record_start('auto_follow_phrase')
	data = follow_phrase()
	record_end(id)
	return data

@shared_task
def auto_unfollow_accounts():
	## Script to automatically and periodically follow accounts
	id = record_start('auto_unfollow')
	data = unfollow_accounts()
	record_end(id)
	return data

@shared_task
def auto_retweet_accounts():
	id = record_start('auto_retweet')
	data = retweet_accounts()
	record_end(id)
	return data

@shared_task
def auto_direct_message_followers():
	id = record_start('auto_direct_message')
	data = direct_message_followers()
	record_end(id)
	return data

@shared_task
def auto_twitter_analyzer():
	id = record_start('auto_twitter_analyzer')
	data = main_app()
	record_end(id)
	return data