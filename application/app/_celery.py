from __future__ import absolute_import, unicode_literals

import os
from celery import Celery
from celery.schedules import crontab
from django.conf import settings

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'app.settings')

	
app = Celery('app')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')
app.autodiscover_tasks()

# Optional configuration, see the application user guide.
app.conf.update(
    result_expires=3600,
)

# @app.on_after_configure.connect
# def setup_periodic_tasks(sender, **kwargs):
	# Calls test('hello') every 10 seconds.
    # sender.add_periodic_task(60.0, app.tasks.auto_follow_accounts.s(), name='auto_follow_every_120')
    # Calls test('world') every 30 seconds
    # sender.add_periodic_task(30.0, app.tasks.add.s(2, 5), expires=10)

    # Executes every Monday morning at 7:30 a.m.
    # sender.add_periodic_task(
    #     crontab(hour=7, minute=30, day_of_week=1),
    #     test.s('Happy Mondays!'),
    # )

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))

if __name__ == '__main__':
    app.start()