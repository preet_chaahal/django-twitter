import json
from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponse
from datetime import datetime, timedelta
from django.http import HttpResponseRedirect
from django_celery_beat.models import PeriodicTask, IntervalSchedule, CrontabSchedule
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib import messages
from django_celery_beat.models import PeriodicTasks
from site_dashboard.models import SiteConfig
from django.conf import settings
from django.contrib.auth.models import Group

def handler404(request):
    response = render_to_response('app/404.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 404
    return response

def handler500(request):
    response = render_to_response('app/500.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 500
    return response

##Site roles and permissions
## Here Group is used to refer roles
def roles_view(request):
	context = {
		'groups': Group.objects.all(),
	}
	return render(request, 'app/pages/roles-view.html', context)

def twitter_tools_settings(request):
	try:
		conf_auto_post = SiteConfig.objects.get(key = 'auto_post')
	except Exception:
		conf_auto_post = False

	try:
		schedule_auto_post = PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_POST)
		schedule_auto_post.crontab.day_of_week = schedule_auto_post.crontab.day_of_week.split(",")
		schedule_auto_post.crontab.month_of_year = schedule_auto_post.crontab.month_of_year.split(",")
		schedule_auto_post.crontab.minute = schedule_auto_post.crontab.minute.split(",")
		schedule_auto_post.crontab.hour = schedule_auto_post.crontab.hour.split(",")
		schedule_auto_post.crontab.time = []
		for i in range(len(schedule_auto_post.crontab.hour)):
		    schedule_auto_post.crontab.time.append(schedule_auto_post.crontab.hour[i]+":"+schedule_auto_post.crontab.minute[i])
	except Exception:
		schedule_auto_post = False

	try:
		conf_auto_follow = SiteConfig.objects.get(key = 'auto_follow')
	except Exception:
		conf_auto_follow = False

	try:
		schedule_auto_follow = PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_FOLLOW)
		schedule_auto_follow.crontab.day_of_week = schedule_auto_follow.crontab.day_of_week.split(",")
		schedule_auto_follow.crontab.month_of_year = schedule_auto_follow.crontab.month_of_year.split(",")
		schedule_auto_follow.crontab.minute = schedule_auto_follow.crontab.minute.split(",")
		schedule_auto_follow.crontab.hour = schedule_auto_follow.crontab.hour.split(",")
		schedule_auto_follow.crontab.time = []
		for i in range(len(schedule_auto_follow.crontab.hour)):
		    schedule_auto_follow.crontab.time.append(schedule_auto_follow.crontab.hour[i]+":"+schedule_auto_follow.crontab.minute[i])
	except Exception:
		schedule_auto_follow = False

	try:
		conf_auto_follow_phrase = SiteConfig.objects.get(key = 'auto_follow_phrase')
	except Exception:
		conf_auto_follow_phrase = False

	try:
		schedule_auto_follow_phrase = PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_FOLLOW_PHRASE)
		schedule_auto_follow_phrase.crontab.day_of_week = schedule_auto_follow_phrase.crontab.day_of_week.split(",")
		schedule_auto_follow_phrase.crontab.month_of_year = schedule_auto_follow_phrase.crontab.month_of_year.split(",")
		schedule_auto_follow_phrase.crontab.minute = schedule_auto_follow_phrase.crontab.minute.split(",")
		schedule_auto_follow_phrase.crontab.hour = schedule_auto_follow_phrase.crontab.hour.split(",")
		schedule_auto_follow_phrase.crontab.time = []
		for i in range(len(schedule_auto_follow_phrase.crontab.hour)):
		    schedule_auto_follow_phrase.crontab.time.append(schedule_auto_follow_phrase.crontab.hour[i]+":"+schedule_auto_follow_phrase.crontab.minute[i])
	except Exception:
		schedule_auto_follow_phrase = False

	try:
		conf_auto_unfollow = SiteConfig.objects.get(key = 'auto_unfollow')
	except Exception:
		conf_auto_unfollow = False

	try:
		schedule_auto_unfollow = PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_UNFOLLOW)
		schedule_auto_unfollow.crontab.day_of_week = schedule_auto_unfollow.crontab.day_of_week.split(",")
		schedule_auto_unfollow.crontab.month_of_year = schedule_auto_unfollow.crontab.month_of_year.split(",")
		schedule_auto_unfollow.crontab.minute = schedule_auto_unfollow.crontab.minute.split(",")
		schedule_auto_unfollow.crontab.hour = schedule_auto_unfollow.crontab.hour.split(",")
		schedule_auto_unfollow.crontab.time = []
		for i in range(len(schedule_auto_unfollow.crontab.hour)):
		    schedule_auto_unfollow.crontab.time.append(schedule_auto_unfollow.crontab.hour[i]+":"+schedule_auto_unfollow.crontab.minute[i])
	except Exception:
		schedule_auto_unfollow = False

	try:
		conf_auto_retweet = SiteConfig.objects.get(key = 'auto_retweet')
	except Exception:
		conf_auto_retweet = False

	try:
		schedule_auto_retweet = PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_RETWEET)
		schedule_auto_retweet.crontab.day_of_week = schedule_auto_retweet.crontab.day_of_week.split(",")
		schedule_auto_retweet.crontab.month_of_year = schedule_auto_retweet.crontab.month_of_year.split(",")
		schedule_auto_retweet.crontab.minute = schedule_auto_retweet.crontab.minute.split(",")
		schedule_auto_retweet.crontab.hour = schedule_auto_retweet.crontab.hour.split(",")
		schedule_auto_retweet.crontab.time = []
		for i in range(len(schedule_auto_retweet.crontab.hour)):
		    schedule_auto_retweet.crontab.time.append(schedule_auto_retweet.crontab.hour[i]+":"+schedule_auto_retweet.crontab.minute[i])
	except Exception:
		schedule_auto_retweet = False

	try:
		conf_auto_direct_message = SiteConfig.objects.get(key = 'auto_direct_message')
	except Exception:
		conf_auto_direct_message = False

	try:
		schedule_auto_direct_message = PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_DIRECT_MESSAGE)
		schedule_auto_direct_message.crontab.day_of_week = schedule_auto_direct_message.crontab.day_of_week.split(",")
		schedule_auto_direct_message.crontab.month_of_year = schedule_auto_direct_message.crontab.month_of_year.split(",")
		schedule_auto_direct_message.crontab.minute = schedule_auto_direct_message.crontab.minute.split(",")
		schedule_auto_direct_message.crontab.hour = schedule_auto_direct_message.crontab.hour.split(",")
		schedule_auto_direct_message.crontab.time = []
		for i in range(len(schedule_auto_direct_message.crontab.hour)):
		    schedule_auto_direct_message.crontab.time.append(schedule_auto_direct_message.crontab.hour[i]+":"+schedule_auto_direct_message.crontab.minute[i])
	except Exception:
		schedule_auto_direct_message = False

	try:
		conf_auto_twitter_analyzer = SiteConfig.objects.get(key = 'auto_twitter_analyzer')
	except Exception:
		conf_auto_twitter_analyzer = False

	try:
		schedule_auto_twitter_analyzer = PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_TWITTER_ANALYZER)
		schedule_auto_twitter_analyzer.crontab.day_of_week = schedule_auto_twitter_analyzer.crontab.day_of_week.split(",")
		schedule_auto_twitter_analyzer.crontab.month_of_year = schedule_auto_twitter_analyzer.crontab.month_of_year.split(",")
		schedule_auto_twitter_analyzer.crontab.minute = schedule_auto_twitter_analyzer.crontab.minute.split(",")
		schedule_auto_twitter_analyzer.crontab.hour = schedule_auto_twitter_analyzer.crontab.hour.split(",")
		schedule_auto_twitter_analyzer.crontab.time = []
		for i in range(len(schedule_auto_twitter_analyzer.crontab.hour)):
		    schedule_auto_twitter_analyzer.crontab.time.append(schedule_auto_twitter_analyzer.crontab.hour[i]+":"+schedule_auto_twitter_analyzer.crontab.minute[i])
	except Exception:
		schedule_auto_twitter_analyzer = False
	
	context = {
		'conf_auto_post': conf_auto_post,
		'schedule_auto_post': schedule_auto_post,
		'conf_auto_follow': conf_auto_follow,
		'schedule_auto_follow': schedule_auto_follow,
		'conf_auto_follow_phrase': conf_auto_follow_phrase,
		'schedule_auto_follow_phrase': schedule_auto_follow_phrase,
		'conf_auto_unfollow': conf_auto_unfollow,
		'schedule_auto_unfollow': schedule_auto_unfollow,
		'conf_auto_retweet': conf_auto_retweet,
		'schedule_auto_retweet': schedule_auto_retweet,
		'conf_auto_direct_message': conf_auto_direct_message,
		'schedule_auto_direct_message': schedule_auto_direct_message,
		'conf_auto_twitter_analyzer': conf_auto_twitter_analyzer,
		'schedule_auto_twitter_analyzer': schedule_auto_twitter_analyzer
	}
	return render(request, "app/config/tools/settings.html", context)

def save_crontask_auto_post():
	try:
		PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_POST).delete()
	except Exception as e:
		flag = False

	cron_minute = "60"
	cron_hour = "1"
	day_of_week = "2"
	day_of_month = "3"
	month_of_year = "4"

	schedule, created = CrontabSchedule.objects.get_or_create(
		minute = cron_minute,
		hour = cron_hour,
		day_of_week=day_of_week,
		day_of_month=day_of_month,
		month_of_year=month_of_year,
	)
	
	PeriodicTask.objects.create(
		name=settings.SCHEDULED_TASK_AUTO_POST,
		crontab=schedule,
		task='app.tasks.auto_post_accounts'
	)

	return True

def save_crontask_auto_follow():
	try:
		PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_FOLLOW).delete()
	except Exception as e:
		flag = False

	cron_minute = "60"
	cron_hour = "1"
	day_of_week = "*"
	day_of_month = "*"
	month_of_year = "*"

	schedule, created = CrontabSchedule.objects.get_or_create(
		minute = cron_minute,
		hour = cron_hour,
		day_of_week=day_of_week,
		day_of_month=day_of_month,
		month_of_year=month_of_year,
	)
	
	PeriodicTask.objects.create(
		name=settings.SCHEDULED_TASK_AUTO_FOLLOW,
		crontab=schedule,
		task='app.tasks.auto_follow_accounts'
	)

	# script_interval = 1800
	# schedule, created = IntervalSchedule.objects.get_or_create(
	# 	every=script_interval,
	# 	period=IntervalSchedule.SECONDS,
	# )
	# PeriodicTask.objects.create(
	# 	interval=schedule,                  # simply describes this periodic task.
	# 	name=settings.SCHEDULED_TASK_AUTO_FOLLOW,          # name of task.
	# 	task='app.tasks.auto_follow_accounts'
	# )
	return True

def save_crontask_auto_retweet():
	try:
		PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_RETWEET).delete()
	except Exception as e:
		flag = False
	
	cron_minute = "60"
	cron_hour = "1"
	day_of_week = "*"
	day_of_month = "*"
	month_of_year = "*"

	schedule, created = CrontabSchedule.objects.get_or_create(
		minute = cron_minute,
		hour = cron_hour,
		day_of_week=day_of_week,
		day_of_month=day_of_month,
		month_of_year=month_of_year,
	)
	
	PeriodicTask.objects.create(
		name=settings.SCHEDULED_TASK_AUTO_RETWEET,
		crontab=schedule,
		task='app.tasks.auto_retweet_accounts'
	)

	# script_interval = 1800
	# schedule, created = IntervalSchedule.objects.get_or_create(
	# 	every=script_interval,
	# 	period=IntervalSchedule.SECONDS,
	# )
	# PeriodicTask.objects.create(
	# 	interval=schedule,                  # simply describes this periodic task.
	# 	name=settings.SCHEDULED_TASK_AUTO_RETWEET,       # name of task. 
	# 	task='app.tasks.auto_retweet_accounts'
	# )
	return True

def save_crontask_auto_direct_message():
	try:
		PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_DIRECT_MESSAGE).delete()
	except Exception as e:
		flag = False
	
	cron_minute = "60"
	cron_hour = "1"
	day_of_week = "*"
	day_of_month = "*"
	month_of_year = "*"

	schedule, created = CrontabSchedule.objects.get_or_create(
		minute = cron_minute,
		hour = cron_hour,
		day_of_week=day_of_week,
		day_of_month=day_of_month,
		month_of_year=month_of_year,
	)
	
	PeriodicTask.objects.create(
		name=settings.SCHEDULED_TASK_AUTO_DIRECT_MESSAGE,
		crontab=schedule,
		task='app.tasks.auto_direct_message_followers'
	)

	# script_interval = 1800
	# schedule, created = IntervalSchedule.objects.get_or_create(
	# 	every=script_interval,
	# 	period=IntervalSchedule.SECONDS,
	# )
	# PeriodicTask.objects.create(
	# 	interval=schedule,                  # simply describes this periodic task.
	# 	name=settings.SCHEDULED_TASK_AUTO_DIRECT_MESSAGE,       # name of task. 
	# 	task='app.tasks.auto_direct_message_followers'
	# )
	return True

def save_crontask_auto_twitter_analyzer():
	try:
		PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_TWITTER_ANALYZER).delete()
	except Exception as e:
		flag = True

	cron_minute = "60"
	cron_hour = "1"
	day_of_week = "*"
	day_of_month = "*"
	month_of_year = "*"

	schedule, created = CrontabSchedule.objects.get_or_create(
		minute = cron_minute,
		hour = cron_hour,
		day_of_week=day_of_week,
		day_of_month=day_of_month,
		month_of_year=month_of_year,
	)
	
	PeriodicTask.objects.create(
		name=settings.SCHEDULED_TASK_AUTO_TWITTER_ANALYZER,
		crontab=schedule,
		task='app.tasks.auto_twitter_analyze'
	)
	# script_interval = 5
	# schedule, created = IntervalSchedule.objects.get_or_create(
	# 	every=script_interval,
	# 	period=IntervalSchedule.HOURS,
	# )
	# PeriodicTask.objects.create(
	# 	interval=schedule,                  # simply describes this periodic task.
	# 	name=settings.SCHEDULED_TASK_AUTO_TWITTER_ANALYZER,       # name of task. 
	# 	task='app.tasks.auto_twitter_analyzer'
	# )
	return True

def save_crontask_auto_follow_phrase():
	try:
		PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_FOLLOW_PHRASE).delete()
	except Exception as e:
		flag = True

	cron_minute = "60"
	cron_hour = "1"
	day_of_week = "*"
	day_of_month = "*"
	month_of_year = "*"

	schedule, created = CrontabSchedule.objects.get_or_create(
		minute = cron_minute,
		hour = cron_hour,
		day_of_week=day_of_week,
		day_of_month=day_of_month,
		month_of_year=month_of_year,
	)
	
	PeriodicTask.objects.create(
		name=settings.SCHEDULED_TASK_AUTO_FOLLOW_PHRASE,
		crontab=schedule,
		task='app.tasks.auto_follow_phrase'
	)
	# script_interval = 6
	# schedule, created = IntervalSchedule.objects.get_or_create(
	# 	every=script_interval,
	# 	period=IntervalSchedule.HOURS,
	# )
	# PeriodicTask.objects.create(
	# 	interval=schedule,                  # simply describes this periodic task.
	# 	name=settings.SCHEDULED_TASK_AUTO_FOLLOW_PHRASE,       # name of task. 
	# 	task='app.tasks.auto_follow_phrase'
	# )
	return True

def save_crontask_auto_unfollow():
	try:
		PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_UNFOLLOW).delete()
	except Exception as e:
		flag = True

	cron_minute = "60"
	cron_hour = "1"
	day_of_week = "*"
	day_of_month = "*"
	month_of_year = "*"

	schedule, created = CrontabSchedule.objects.get_or_create(
		minute = cron_minute,
		hour = cron_hour,
		day_of_week=day_of_week,
		day_of_month=day_of_month,
		month_of_year=month_of_year,
	)
	
	PeriodicTask.objects.create(
		name=settings.SCHEDULED_TASK_AUTO_UNFOLLOW,
		crontab=schedule,
		task='app.tasks.auto_unfollow_accounts'
	)
	# script_interval = 5
	# schedule, created = IntervalSchedule.objects.get_or_create(
	# 	every=script_interval,
	# 	period=IntervalSchedule.HOURS,
	# )
	# PeriodicTask.objects.create(
	# 	interval=schedule,                  # simply describes this periodic task.
	# 	name=settings.SCHEDULED_TASK_AUTO_UNFOLLOW,       # name of task. 
	# 	task='app.tasks.auto_unfollow_accounts'
	# )
	return True

##Resetting all cron tasks
def reset_all_automatic_tasks(request):
	flag = False
	save_crontask_auto_post()
	save_crontask_auto_follow()
	save_crontask_auto_retweet()
	save_crontask_auto_direct_message()
	save_crontask_auto_twitter_analyzer()
	save_crontask_auto_follow_phrase()
	save_crontask_auto_unfollow()
	
	if not flag:
		messages.add_message(request, messages.SUCCESS, 'Automatic background tasks restored to default state!')
	else:
		messages.add_message(request, messages.ERROR, 'Something went wrong trying to restore the default background tasks schedule!')
	return HttpResponseRedirect(reverse('home'))
	# return HttpResponseRedirect(reverse("twitter_tools_settings"))


## Toosl Config
def config_tools_auto_retweet(request):
	context = {
		'conf': SiteConfig.objects.get(key = 'auto_retweet'),
		'schedule': PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_RETWEET)
	}
	return render(request, 'app/config/tools/auto-retweet.html', context)
	# return HttpResponseRedirect(reverse("twitter_auto_direct_message_account", args=(), kwargs={'id':request.POST['id']})

def config_tools_auto_retweet_update_config(request):
	if request.POST['tool'] == 'auto_retweet':
		if request.POST['tool_status'] == 'active':
			tool_status = True
		else:
			tool_status = False
		
		if request.POST['skip_duplicate_retweet'] == 'active':
			skip_duplicate_retweet = True
		else:
			skip_duplicate_retweet = False

		elem = SiteConfig.objects.get(key = 'auto_retweet')
		elem.config['tool_status'] = tool_status
		elem.config['skip_duplicate_retweet'] = skip_duplicate_retweet
		elem.save()
		
		p_task = PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_RETWEET)
		p_task.enabled=tool_status
		p_task.save()

		messages.add_message(request, messages.SUCCESS, 'Tools Configuration updated!')

		return HttpResponseRedirect(reverse("twitter_tools_settings"))
	else:
		return HttpResponse('Invalid request!')

## Updating the schedule for auto_retweet schedule
def config_tools_auto_retweet_update_schedule(request):
	if request.POST['tool'] == 'auto_retweet':
		cron_minute = "60"
		cron_hour = "*"
		cron_day_of_week = "*"
		cron_month_of_year = "*"
		
		i=0
		total_times = len(request.POST.getlist('start_time')) - 1

		for start_time in request.POST.getlist('start_time'):
			if i == 0:
				cron_minute = start_time.split(":")[1]
				cron_hour = start_time.split(":")[0]
			else:
				cron_minute += start_time.split(":")[1] 
				cron_hour += start_time.split(":")[0] 

			if i < total_times:
				cron_minute += ","
				cron_hour += ","
			i += 1

		i=0
		total_days = len(request.POST.getlist('day_of_week')) - 1
		for day_of_week in request.POST.getlist('day_of_week'):
			if i == 0:
				cron_day_of_week = day_of_week 	
			else:
				cron_day_of_week += day_of_week 
			if i < total_days:
				cron_day_of_week += ","
			i += 1

		i=0
		total_months = len(request.POST.getlist('month_of_year')) - 1
		for month_of_year in request.POST.getlist('month_of_year'):
			if i == 0:
				cron_month_of_year = month_of_year 
			else:
				cron_month_of_year += month_of_year
			if i < total_months:
				cron_month_of_year += ","
			i += 1
		
		schedule, created = CrontabSchedule.objects.get_or_create(
			minute = cron_minute,
			hour = cron_hour,
			day_of_week=cron_day_of_week,
			month_of_year=cron_month_of_year,
		)
		
		p_task = PeriodicTask.objects.get(
			name=settings.SCHEDULED_TASK_AUTO_RETWEET,
		)
		p_task.crontab=schedule
		p_task.save()

		messages.add_message(request, messages.SUCCESS, 'Auto Retweet Tool schedule updated!')
		return HttpResponseRedirect(reverse("twitter_tools_settings"))
	else:
		return HttpResponse('Invalid request!')


def config_tools_auto_post(request):
	return render(request, 'app/config/tools/auto-post.html')
	# return HttpResponseRedirect(reverse("twitter_auto_direct_message_account", args=(), kwargs={'id':request.POST['id']})

def config_tools_auto_post_update_config(request):
	if request.POST['tool'] == 'auto_post':
		
		if request.POST['tool_status'] == 'active':
			tool_status = True
		else:
			tool_status = False

		elem = SiteConfig.objects.get(key = 'auto_post')
		elem.config['tool_status'] = tool_status
		elem.config['max_post_limit'] = request.POST['max_post_limit']
		elem.config['post_limit_per_run'] = request.POST['post_limit_per_run']
		elem.save()

		p_task = PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_RETWEET)
		p_task.enabled=tool_status
		p_task.save()

		messages.add_message(request, messages.SUCCESS, 'Tools Configuration updated!')
		return HttpResponseRedirect(reverse("twitter_tools_settings"))
	else:
		return HttpResponse('Invalid request!')

## Updating the schedule for auto_post schedule
def config_tools_auto_post_update_schedule(request):
	if request.POST.get('tool') == 'auto_post':
		
		cron_minute = "60"
		cron_hour = "*"
		cron_day_of_week = "*"
		cron_month_of_year = "*"
		
		i=0
		total_times = len(request.POST.getlist('start_time')) - 1

		for start_time in request.POST.getlist('start_time'):
			if i == 0:
				cron_minute = start_time.split(":")[1]
				cron_hour = start_time.split(":")[0]
			else:
				cron_minute += start_time.split(":")[1] 
				cron_hour += start_time.split(":")[0] 

			if i < total_times:
				cron_minute += ","
				cron_hour += ","
			i += 1

		i=0
		total_days = len(request.POST.getlist('day_of_week')) - 1
		for day_of_week in request.POST.getlist('day_of_week'):
			if i == 0:
				cron_day_of_week = day_of_week 	
			else:
				cron_day_of_week += day_of_week 
			if i < total_days:
				cron_day_of_week += ","
			i += 1

		i=0
		total_months = len(request.POST.getlist('month_of_year')) - 1
		for month_of_year in request.POST.getlist('month_of_year'):
			if i == 0:
				cron_month_of_year = month_of_year 
			else:
				cron_month_of_year += month_of_year
			if i < total_months:
				cron_month_of_year += ","
			i += 1
		
		schedule, created = CrontabSchedule.objects.get_or_create(
			minute = cron_minute,
			hour = cron_hour,
			day_of_week=cron_day_of_week,
			month_of_year=cron_month_of_year,
		)
		
		p_task = PeriodicTask.objects.get(
			name=settings.SCHEDULED_TASK_AUTO_POST,
		)
		p_task.crontab=schedule
		p_task.save()

		messages.add_message(request, messages.SUCCESS, 'Auto Post Tool schedule updated!')
		return HttpResponseRedirect(reverse("twitter_tools_settings"))
	else:
		return HttpResponse('Invalid request!')

def config_tools_auto_follow(request):
	context = {
		'conf': SiteConfig.objects.get(key = 'auto_follow'),
		'schedule': PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_FOLLOW)
	}
	return render(request, 'app/config/tools/auto-follow.html', context)
	# return HttpResponseRedirect(reverse("twitter_auto_direct_message_account", args=(), kwargs={'id':request.POST['id']})

def config_tools_auto_follow_update_config(request):
	if request.POST['tool'] == 'auto_follow':
		if request.POST['tool_status'] == 'active':
			tool_status = True
		else:
			tool_status = False
		elem = SiteConfig.objects.get(key = 'auto_follow')
		elem.config['tool_status'] = tool_status
		elem.config['max_follow_limit'] = request.POST['max_follow_limit']
		elem.config['follow_limit_per_run'] = request.POST['follow_limit_per_run']
		elem.save()

		p_task = PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_FOLLOW)
		p_task.enabled=tool_status
		p_task.save()

		messages.add_message(request, messages.SUCCESS, 'Tools Configuration updated!')
		return HttpResponseRedirect(reverse("twitter_tools_settings"))
	else:
		return HttpResponse('Invalid request!')

## Updating the schedule for auto_follow schedule
def config_tools_auto_follow_update_schedule(request):
	if request.POST['tool'] == 'auto_follow':
		cron_minute = "60"
		cron_hour = "*"
		cron_day_of_week = "*"
		cron_month_of_year = "*"
		
		i=0
		total_times = len(request.POST.getlist('start_time')) - 1

		for start_time in request.POST.getlist('start_time'):
			if i == 0:
				cron_minute = start_time.split(":")[1]
				cron_hour = start_time.split(":")[0]
			else:
				cron_minute += start_time.split(":")[1] 
				cron_hour += start_time.split(":")[0] 

			if i < total_times:
				cron_minute += ","
				cron_hour += ","
			i += 1

		i=0
		total_days = len(request.POST.getlist('day_of_week')) - 1
		for day_of_week in request.POST.getlist('day_of_week'):
			if i == 0:
				cron_day_of_week = day_of_week 	
			else:
				cron_day_of_week += day_of_week 
			if i < total_days:
				cron_day_of_week += ","
			i += 1

		i=0
		total_months = len(request.POST.getlist('month_of_year')) - 1
		for month_of_year in request.POST.getlist('month_of_year'):
			if i == 0:
				cron_month_of_year = month_of_year 
			else:
				cron_month_of_year += month_of_year
			if i < total_months:
				cron_month_of_year += ","
			i += 1
		
		schedule, created = CrontabSchedule.objects.get_or_create(
			minute = cron_minute,
			hour = cron_hour,
			day_of_week=cron_day_of_week,
			month_of_year=cron_month_of_year,
		)
		
		p_task = PeriodicTask.objects.get(
			name=settings.SCHEDULED_TASK_AUTO_FOLLOW,
		)
		p_task.crontab=schedule
		p_task.save()

		messages.add_message(request, messages.SUCCESS, 'Auto Follow Tool schedule updated!')
		return HttpResponseRedirect(reverse("twitter_tools_settings"))
	else:
		return HttpResponse('Invalid request!')

##Auto follow phrase
def config_tools_auto_follow_phrase_update_config(request):
	if request.POST['tool'] == 'auto_follow_phrase':
		if request.POST['tool_status'] == 'active':
			tool_status = True
		else:
			tool_status = False
		elem = SiteConfig.objects.get(key = 'auto_follow_phrase')
		elem.config['tool_status'] = tool_status
		elem.config['follow_limit_per_run'] = request.POST['follow_limit_per_run']
		elem.save()

		p_task = PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_FOLLOW_PHRASE)
		p_task.enabled=tool_status
		p_task.save()

		messages.add_message(request, messages.SUCCESS, 'Tools Configuration updated!')
		return HttpResponseRedirect(reverse("twitter_tools_settings"))
	else:
		return HttpResponse('Invalid request!')

## Updating the schedule for auto_follow schedule
def config_tools_auto_follow_phrase_update_schedule(request):
	if request.POST['tool'] == 'auto_follow_phrase':
		cron_minute = "60"
		cron_hour = "*"
		cron_day_of_week = "*"
		cron_month_of_year = "*"
		
		i=0
		total_times = len(request.POST.getlist('start_time')) - 1

		for start_time in request.POST.getlist('start_time'):
			if i == 0:
				cron_minute = start_time.split(":")[1]
				cron_hour = start_time.split(":")[0]
			else:
				cron_minute += start_time.split(":")[1] 
				cron_hour += start_time.split(":")[0] 

			if i < total_times:
				cron_minute += ","
				cron_hour += ","
			i += 1

		i=0
		total_days = len(request.POST.getlist('day_of_week')) - 1
		for day_of_week in request.POST.getlist('day_of_week'):
			if i == 0:
				cron_day_of_week = day_of_week 	
			else:
				cron_day_of_week += day_of_week 
			if i < total_days:
				cron_day_of_week += ","
			i += 1

		i=0
		total_months = len(request.POST.getlist('month_of_year')) - 1
		for month_of_year in request.POST.getlist('month_of_year'):
			if i == 0:
				cron_month_of_year = month_of_year 
			else:
				cron_month_of_year += month_of_year
			if i < total_months:
				cron_month_of_year += ","
			i += 1
		
		schedule, created = CrontabSchedule.objects.get_or_create(
			minute = cron_minute,
			hour = cron_hour,
			day_of_week=cron_day_of_week,
			month_of_year=cron_month_of_year,
		)
		
		p_task = PeriodicTask.objects.get(
			name=settings.SCHEDULED_TASK_AUTO_FOLLOW_PHRASE,
		)
		p_task.crontab=schedule
		p_task.save()

		messages.add_message(request, messages.SUCCESS, 'Auto Follow Phrase Tool schedule updated!')
		return HttpResponseRedirect(reverse("twitter_tools_settings"))
	else:
		return HttpResponse('Invalid request!')

##Auto unfollow phrase
def config_tools_auto_unfollow_update_config(request):
	if request.POST['tool'] == 'auto_unfollow':
		if request.POST['tool_status'] == 'active':
			tool_status = True
		else:
			tool_status = False
		elem = SiteConfig.objects.get(key = 'auto_unfollow')
		elem.config['tool_status'] = tool_status
		elem.config['unfollow_limit_per_run'] = request.POST.get('unfollow_limit_per_run')
		elem.save()

		p_task = PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_UNFOLLOW)
		p_task.enabled=tool_status
		p_task.save()

		messages.add_message(request, messages.SUCCESS, 'Tools Configuration updated!')
		return HttpResponseRedirect(reverse("twitter_tools_settings"))
	else:
		return HttpResponse('Invalid request!')

## Updating the schedule for auto_follow schedule
def config_tools_auto_unfollow_update_schedule(request):
	if request.POST['tool'] == 'auto_unfollow':
		cron_minute = "60"
		cron_hour = "*"
		cron_day_of_week = "*"
		cron_month_of_year = "*"
		
		i=0
		total_times = len(request.POST.getlist('start_time')) - 1

		for start_time in request.POST.getlist('start_time'):
			if i == 0:
				cron_minute = start_time.split(":")[1]
				cron_hour = start_time.split(":")[0]
			else:
				cron_minute += start_time.split(":")[1] 
				cron_hour += start_time.split(":")[0] 

			if i < total_times:
				cron_minute += ","
				cron_hour += ","
			i += 1

		i=0
		total_days = len(request.POST.getlist('day_of_week')) - 1
		for day_of_week in request.POST.getlist('day_of_week'):
			if i == 0:
				cron_day_of_week = day_of_week 	
			else:
				cron_day_of_week += day_of_week 
			if i < total_days:
				cron_day_of_week += ","
			i += 1

		i=0
		total_months = len(request.POST.getlist('month_of_year')) - 1
		for month_of_year in request.POST.getlist('month_of_year'):
			if i == 0:
				cron_month_of_year = month_of_year 
			else:
				cron_month_of_year += month_of_year
			if i < total_months:
				cron_month_of_year += ","
			i += 1
		
		schedule, created = CrontabSchedule.objects.get_or_create(
			minute = cron_minute,
			hour = cron_hour,
			day_of_week=cron_day_of_week,
			month_of_year=cron_month_of_year,
		)
		
		p_task = PeriodicTask.objects.get(
			name=settings.SCHEDULED_TASK_AUTO_UNFOLLOW,
		)
		p_task.crontab=schedule
		p_task.save()

		messages.add_message(request, messages.SUCCESS, 'Auto Unfollow Tool schedule updated!')
		return HttpResponseRedirect(reverse("twitter_tools_settings"))
	else:
		return HttpResponse('Invalid request!')

def config_tools_auto_direct_message(request):
	context = {
		'conf': SiteConfig.objects.get(key = 'auto_direct_message'),
		'schedule': PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_DIRECT_MESSAGE)
	}
	return render(request, 'app/config/tools/auto-direct-message.html', context)
	# return HttpResponseRedirect(reverse("twitter_auto_direct_message_account", args=(), kwargs={'id':request.POST['id']})

def config_tools_auto_direct_message_update_config(request):
	if request.POST['tool'] == 'auto_direct_message':
		
		if request.POST['tool_status'] == 'active':
			tool_status = True
		else:
			tool_status = False

		if request.POST['enable_clearing_direct_messages'] == 'active':
			enable_clearing_direct_messages = True
		else:
			enable_clearing_direct_messages = False
		
		elem = SiteConfig.objects.get(key = 'auto_direct_message')
		elem.config['tool_status'] = tool_status
		elem.config['enable_clearing_direct_messages'] = enable_clearing_direct_messages
		elem.save()

		p_task = PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_DIRECT_MESSAGE)
		p_task.enabled=tool_status
		p_task.save()

		messages.add_message(request, messages.SUCCESS, 'Tools Configuration updated!')
		return HttpResponseRedirect(reverse("twitter_tools_settings"))
	else:
		return HttpResponse('Invalid request!')

## Updating the schedule for auto_direct_message schedule
def config_tools_auto_direct_message_update_schedule(request):
	if request.POST['tool'] == 'auto_direct_message':
		cron_minute = "60"
		cron_hour = "*"
		cron_day_of_week = "*"
		cron_month_of_year = "*"
		
		i=0
		total_times = len(request.POST.getlist('start_time')) - 1

		for start_time in request.POST.getlist('start_time'):
			if i == 0:
				cron_minute = start_time.split(":")[1]
				cron_hour = start_time.split(":")[0]
			else:
				cron_minute += start_time.split(":")[1] 
				cron_hour += start_time.split(":")[0] 

			if i < total_times:
				cron_minute += ","
				cron_hour += ","
			i += 1

		i=0
		total_days = len(request.POST.getlist('day_of_week')) - 1
		for day_of_week in request.POST.getlist('day_of_week'):
			if i == 0:
				cron_day_of_week = day_of_week 	
			else:
				cron_day_of_week += day_of_week 
			if i < total_days:
				cron_day_of_week += ","
			i += 1

		i=0
		total_months = len(request.POST.getlist('month_of_year')) - 1
		for month_of_year in request.POST.getlist('month_of_year'):
			if i == 0:
				cron_month_of_year = month_of_year 
			else:
				cron_month_of_year += month_of_year
			if i < total_months:
				cron_month_of_year += ","
			i += 1
		
		schedule, created = CrontabSchedule.objects.get_or_create(
			minute = cron_minute,
			hour = cron_hour,
			day_of_week=cron_day_of_week,
			month_of_year=cron_month_of_year,
		)
		
		p_task = PeriodicTask.objects.get(
			name=settings.SCHEDULED_TASK_AUTO_DIRECT_MESSAGE,
		)
		p_task.crontab=schedule
		p_task.save()

		messages.add_message(request, messages.SUCCESS, 'Auto Direct Message Tool schedule updated!')
		return HttpResponseRedirect(reverse("twitter_tools_settings"))
	else:
		return HttpResponse('Invalid request!')


def config_tools_auto_twitter_analyzer(request):
	context = {
		'conf': SiteConfig.objects.get(key = 'auto_twitter_analyzer'),
		'schedule': PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_TWITTER_ANALYZER)
	}
	return render(request, 'app/config/tools/auto-twitter-analyzer.html', context)
	# return HttpResponseRedirect(reverse("twitter_auto_direct_message_account", args=(), kwargs={'id':request.POST['id']})

def config_tools_auto_twitter_analyzer_update_config(request):
	if request.POST['tool'] == 'auto_twitter_analyzer':
		
		if request.POST['tool_status'] == 'active':
			tool_status = True
		else:
			tool_status = False

		elem = SiteConfig.objects.get(key = 'auto_twitter_analyzer')
		elem.config['tool_status'] = tool_status
		elem.config['max_limit_for_tweet_analysis'] = request.POST['max_limit_for_tweet_analysis']
		elem.save()

		p_task = PeriodicTask.objects.get(name=settings.SCHEDULED_TASK_AUTO_TWITTER_ANALYZER)
		p_task.enabled=tool_status
		p_task.save()

		messages.add_message(request, messages.SUCCESS, 'Tools Configuration updated!')
		return HttpResponseRedirect(reverse("twitter_tools_settings"))
	else:
		return HttpResponse('Invalid request!')

## Updating the schedule for auto_direct_message schedule
def config_tools_auto_twitter_analyzer_update_schedule(request):
	
	if request.POST['tool'] == 'auto_twitter_analyzer':
		cron_minute = "60"
		cron_hour = "*"
		cron_day_of_week = "*"
		cron_month_of_year = "*"
		
		i=0
		total_times = len(request.POST.getlist('start_time')) - 1

		for start_time in request.POST.getlist('start_time'):
			if i == 0:
				cron_minute = start_time.split(":")[1]
				cron_hour = start_time.split(":")[0]
			else:
				cron_minute += start_time.split(":")[1] 
				cron_hour += start_time.split(":")[0] 

			if i < total_times:
				cron_minute += ","
				cron_hour += ","
			i += 1

		i=0
		total_days = len(request.POST.getlist('day_of_week')) - 1
		for day_of_week in request.POST.getlist('day_of_week'):
			if i == 0:
				cron_day_of_week = day_of_week 	
			else:
				cron_day_of_week += day_of_week 
			if i < total_days:
				cron_day_of_week += ","
			i += 1

		i=0
		total_months = len(request.POST.getlist('month_of_year')) - 1
		for month_of_year in request.POST.getlist('month_of_year'):
			if i == 0:
				cron_month_of_year = month_of_year 
			else:
				cron_month_of_year += month_of_year
			if i < total_months:
				cron_month_of_year += ","
			i += 1
		
		schedule, created = CrontabSchedule.objects.get_or_create(
			minute = cron_minute,
			hour = cron_hour,
			day_of_week=cron_day_of_week,
			month_of_year=cron_month_of_year,
		)
		
		p_task = PeriodicTask.objects.get(
			name=settings.SCHEDULED_TASK_AUTO_TWITTER_ANALYZER,
		)
		p_task.crontab=schedule
		p_task.save()

		messages.add_message(request, messages.SUCCESS, 'Auto Twitter Analyzer Tool schedule updated!')
		return HttpResponseRedirect(reverse("twitter_tools_settings"))
	else:
		return HttpResponse('Invalid request!')

def config_tools_reset(request):
	## Deleting all objects
	try:
		SiteConfig.objects.filter(config_type = 'T').delete()
	except Exception as e:
		flag = False

	##STATIC SETTINGS
	SiteConfig.objects.create(key='auto_post', config_type='T', config={
			'tool_status': True,
			'post_limit_per_run': 50,
			'max_post_limit': 200
		})
	SiteConfig.objects.create(key='auto_follow', config_type='T', config={
			'tool_status': True,
			'follow_limit_per_run': 10,
			'max_follow_limit': 100
		})
	SiteConfig.objects.create(key='auto_retweet', config_type='T', config={
			'tool_status': True,
			'skip_duplicate_retweet': True
		})
	SiteConfig.objects.create(key='auto_direct_message', config_type='T', config={
			'tool_status': True,
			'enable_clearing_direct_messages': True
		})
	SiteConfig.objects.create(key='auto_twitter_analyzer', config_type='T', config={
			'tool_status': True,
			'max_limit_for_tweet_analysis': 50
		})
	SiteConfig.objects.create(key='auto_follow_phrase', config_type='T', config={
			'tool_status': True,
			'follow_limit_per_run': 10,
		})
	SiteConfig.objects.create(key='auto_unfollow', config_type='T', config={
			'tool_status': True,
			'unfollow_limit_per_run': 10,
		})
	messages.add_message(request, messages.SUCCESS, 'Tools Configurations restored to factory settings!')
	return HttpResponseRedirect(reverse("twitter_tools_settings"))