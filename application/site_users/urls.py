from django.conf.urls import url

from . import views

urlpatterns = [
	url(r'^users/add/', views.users_add, name='users_add'),
	url(r'^users/view/', views.users_view, name='users_view'),
	url(r'^users/edit/(?P<id>[0-9]+)/', views.users_edit, name='users_edit'),
]