from django.urls import reverse
from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from .forms import AddUserForm
from .forms import UpdateUserForm
from django.contrib import messages
from django.contrib.auth.hashers import make_password
import tweepy

#User management related methods
def users_add(request):
	if request.method == 'POST':
		form = AddUserForm(request.POST)
		if form.is_valid():
			t_user = form.save(commit=False)
			t_user.is_active = True
			t_user.password = make_password(request.POST['password'])
			t_user.is_superuser = False
			t_user.save()
			context = {
				'status': 1,
				'message': 'User account added!'
			}
			return render(request, "site_users/add-user.html", context)

	context = {
		'users': User.objects.all(),
		'addUserForm': AddUserForm()
	}
	return render(request, "site_users/add-user.html", context)

def users_view(request):
	context = {
		'users': User.objects.all()
	}
	return render(request, "site_users/view-user.html", context)

#User management related methods
def users_edit(request, id):
	if request.method == 'POST':
		existing_obj = User.objects.get(pk=request.POST['id'])
		form = UpdateUserForm(request.POST, instance = existing_obj)
		if form.is_valid():
			t_user = form.save(commit=False)
			t_user.is_active = True
			t_user.password = make_password(request.POST['password'])
			t_user.is_superuser = False
			t_user.save()
			context = {
				'status': 1,
				'message': 'User account added!'
			}
			messages.add_message(request, messages.SUCCESS, 'User data updated!')
			return HttpResponseRedirect(reverse('users_view'))
		else:
			messages.add_message(request, messages.ERROR, 'Invalid form entries')

	context = {
		'user': User.objects.get(id = id),
		'updateUserForm': UpdateUserForm()
	}
	return render(request, "site_users/edit-user.html", context)