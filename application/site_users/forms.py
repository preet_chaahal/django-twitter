from django import forms
from django.contrib.auth.models import User

class AddUserForm(forms.ModelForm):
	class Meta:
		model = User
		fields = ('username', 'first_name', 'last_name', 'email', 'is_staff', 'password',  )

class UpdateUserForm(forms.ModelForm):
	class Meta:
		model = User
		fields = ('username', 'first_name', 'last_name', 'email', 'is_staff', 'password', 'id')